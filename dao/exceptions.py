import logging

logger = logging.getLogger(__name__)

__all__ = ['DoesNotExist', 'SocketClosed']


class DoesNotExist(Exception):
    """A generic does-not-exist exception"""


class SocketClosed(Exception):
    """
    A send() attempt was made to a closed socket
    """
