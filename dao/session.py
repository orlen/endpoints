import time
import typing as tp
import uuid

from satella.coding import short_none

from endpoints.cassandra import Cassandra
from .exceptions import DoesNotExist
from .user import User

__all__ = ['Session']


class Session:
    class DoesNotExist(DoesNotExist):
        pass

    class Expired(DoesNotExist):
        pass

    def __init__(self, session_id: str):
        self.session_id = session_id
        with Cassandra() as cur:
            p = cur.execute('SELECT user_id, expiry_time FROM sessions WHERE session_id=%s', (session_id,))

        if not p:
            raise Session.DoesNotExist(session_id)

        self.user_id, self.expiry_time = p[0]

        if self.expiry_time is not None and time.time() > self.expiry_time:
            self.delete()
            raise Session.Expired()

    @property
    def user(self) -> User:
        return User(self.user_id)

    @staticmethod
    def create(user: tp.Union[User, str], expiry_time: tp.Optional[float] = None) -> str:
        """Creates a session, returns a session ID"""
        session_id = uuid.uuid4().hex
        expiry_time = short_none(int)(expiry_time)

        now = time.time()
        assert expiry_time is not None and expiry_time > now

        if isinstance(user, User):
            user_id = user.user_id
        else:
            user_id = user

        que = 'INSERT INTO sessions (session_id, user_id, expiry_time) VALUES (%s, %s, %s)'
        args = (session_id, user_id, expiry_time)

        if expiry_time is not None:
            que = que + ' USING TTL %s'
            args = args + (int(expiry_time - now), )

        with Cassandra() as cur:
            cur.execute(que, args)

        return session_id

    def extend_for(self, extend_by: tp.Optional[float] = None, extend_to: tp.Optional[float] = None) -> None:
        """
        You can use only one of the provided parameters.

        :param extend_by: amount of seconds to extend the session by
        :param extend_to: new expiration timestamp
        :raises ValueError: both parameters were given
        """
        if extend_by is not None and extend_to is not None:
            raise ValueError('You can provide at most a single parameter!')

        if self.expiry_time is None and extend_to is None:
            return  # it is pointless to extend a never expiring session

        if extend_by is not None:
            self.expiry_time += int(extend_by)
        if extend_to is not None:
            self.expiry_time = int(extend_to)

        now = time.time()
        assert self.expiry_time > now, 'Expiration time in the past!'
        ttl = int(self.expiry_time - now)

        with Cassandra() as cur:
            cur.execute('UPDATE sessions USING TTL %s SET user_id=%s, expiry_time=%s WHERE session_id=%s',
                        (ttl, self.user_id, self.expiry_time, self.session_id))

    def delete(self) -> None:
        with Cassandra() as cur:
            cur.execute("DELETE FROM sessions WHERE session_id=%s", (self.session_id,))
