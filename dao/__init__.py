from satella.json import JSONAble
from .device import Device, DeviceDataFileAttached
from .exceptions import SocketClosed
from .failure import Failure
from .flight import Flight, DataFileAttached, DailyScheduled
from .session import Session
from .user import User
from .weather_data import WeatherDataPoint

__all__ = ['Flight', 'Session', 'User', 'DataFileAttached',
           'Device', 'DeviceDataFileAttached',
           'DailyScheduled', 'Failure',
           'SocketClosed', 'WeatherDataPoint']

"""
All data access objects used by the Endpoints app
Every call to the database is made through here
So only this module (and endpoints) is allowed to use
Redis and Cassandra
"""
