import logging
import typing as tp
import uuid
import os

from endpoints.redis import redis, redis_plain_get
from .device import Device
from endpoints.cassandra import Cassandra
from endpoints.settings import VIDEO_PATH
from satella.json import JSONAble
from .exceptions import DoesNotExist
from .user import User

__all__ = ['Flight', 'DataFileAttached', 'DailyScheduled']
logger = logging.getLogger(__name__)


class Flight(JSONAble):
    TOM_AUTOMATIC = 0
    TOM_MANUAL = 1

    APROV_APPROVED = 1
    APROV_REJECTED = 2
    APROV_NOT_CONSIDERED = None

    STATUS_WAITING = None             # waiting to be picked up by the schedule runner
    STATUS_INITIALIZED = 0            # information sent to the drone, waiting for drone to pick up the flight
    STATUS_PENDING_CONFIRMATION = 1   # pending confirmation by the operator
    STATUS_CONFIRMED_OPERATOR = 2     # flight has been accepted by the operator and is in progress
    STATUS_TERMINATED = 3             # flight has been completed
    STATUS_CANCELLED = 4              # flight has been rejected by the operator

    class DoesNotExist(DoesNotExist):
        pass

    @staticmethod
    def choose(start: float, stop: float, order_by: str = 'ASC', limit: tp.Union[int, None] = None) -> tp.List[
            'Flight']:
        args = ('a', int(start), int(stop),)
        query = 'SELECT flight_id FROM flights_by_datetime WHERE pk=%s AND datetime >= %s AND datetime <= %s ORDER BY datetime ' + order_by

        if limit is not None:
            query = query + ' LIMIT %s'
            args = args + (limit,)

        with Cassandra() as cur:
            q = cur.execute(query, args)
        x: tp.List[Flight] = [Flight(p) for p, in q]
        return x

    def refresh(self):
        with Cassandra() as cur:
            q = cur.execute(
                'SELECT user_id, datetime, drone_id, hangar_id, whom_approved, approved, type_of_mission, status, datafile_ids, stream_port FROM flights WHERE flight_id=%s',
                (self.flight_id,))

        if not q:
            raise Flight.DoesNotExist(self.flight_id)

        self.user_id, self.datetime, self.drone_id, self.hangar_id, self.whom_approved, self.approved, self.type_of_mission, self._status, self.datafile_ids, self._stream_port = \
            q[0]
        if self.datafile_ids is None:
            self.datafile_ids = set()

        self._drone = None
        self._hangar = None

    @property
    def drone(self) -> Device:
        if self._drone is not None:
            return self._drone

        if self.drone_id is None:
            self.refresh()

        self._drone = Device(self.drone_id)
        return self._drone

    @property
    def hangar(self) -> Device:
        if self._hangar is not None:
            return self._hangar

        if self.hangar_id is None:
            self.refresh()
        self._hangar = Device(self.hangar_id)
        return self._hangar

    @drone.setter
    def drone(self, new_drone: tp.Union[Device, str]) -> None:
        self._drone = None

        if isinstance(new_drone, Device):
            new_drone = new_drone.device_id

        with Cassandra() as cur:
            cur.execute('UPDATE flights SET drone_id=%s WHERE flight_id=%s', (new_drone, self.flight_id))
        self.drone_id = new_drone

    @hangar.setter
    def hangar(self, new_hangar: tp.Union[Device, str]) -> None:
        self._hangar = None
        if isinstance(new_hangar, Device):
            new_hangar = new_hangar.device_id

        with Cassandra() as cur:
            cur.execute('UPDATE flights SET hangar_id=%s WHERE flight_id=%s', (new_hangar, self.flight_id))
        self.hangar_id = new_hangar

    def delete(self) -> None:
        self.refresh()

        redis.hdel('FLIGHTS', self.flight_id)

        def check_and_delete(key, value):
            if redis_plain_get(key, stfu=True) == value:
                redis.delete(key)

        check_and_delete('HANGAR.%s.FLIGHT' % (self.hangar_id, ), self.flight_id)
        check_and_delete('DRONE.%s.FLIGHT' % (self.drone_id, ), self.flight_id)
        check_and_delete('USER.%s.FLIGHT' % (self.user_id, ), self.flight_id)
        redis.delete('FLIGHT.%s.SECONDS_REMAINING_IN_WAIT' % (self.flight_id, ))
        redis.delete('FLIGHT.%s.LAST_CHECKED' % (self.flight_id, ))

        for datafile_id in self.datafile_ids:
            DataFileAttached(datafile_id).delete()

        with Cassandra.batch() as cur:
            cur.execute('DELETE FROM flights_by_datetime WHERE pk=%s AND datetime=%s',
                        ('a', self.datetime))
            cur.execute('DELETE FROM flights_by_datetime WHERE pk=%s AND datetime=%s',
                        (self.drone_id, self.datetime))
            cur.execute('DELETE FROM flights_by_datetime WHERE pk=%s AND datetime=%s',
                        (self.hangar_id, self.datetime))
            cur.execute('DELETE FROM flights WHERE flight_id=%s', (self.flight_id,))

        try:
            os.unlink(os.path.join(VIDEO_PATH, '%s.mp4' % (self.flight_id, )))
        except OSError:
            pass

    @staticmethod
    def create(dt: int, drone_id: tp.Union[str, Device], hangar_id: tp.Union[str, Device],
               user_id: tp.Union[str, User], type_of_mission: int) -> str:
        """
        :param dt: UNIX timestamp of the flight taking place
        :param drone_id: Used drone's ID
        :param hangar_id: Used hangar's ID
        :param user_id: User ID
        :param type_of_mission: Type of mission
        :return: a new flight ID
        """
        if isinstance(drone_id, Device):
            drone_id = drone_id.device_id
        if isinstance(hangar_id, Device):
            hangar_id = hangar_id.device_id
        if isinstance(user_id, User):
            user_id = user_id.user_id

        flight_id = uuid.uuid4().hex

        assert type_of_mission in (Flight.TOM_AUTOMATIC, Flight.TOM_MANUAL)

        with Cassandra.batch() as cur:
            cur.execute(
                'INSERT INTO flights (flight_id, datetime, drone_id, hangar_id, type_of_mission, user_id) VALUES (%s, %s, %s, %s, %s, %s)',
                (flight_id, dt, drone_id, hangar_id, type_of_mission, user_id))
            cur.execute('INSERT INTO flights_by_datetime (flight_id, datetime, pk) VALUES (%s, %s, %s)',
                        (flight_id, dt, 'a'))
            cur.execute('INSERT INTO flights_by_datetime (flight_id, datetime, pk) VALUES (%s, %s, %s)',
                        (flight_id, dt, drone_id))
            cur.execute('INSERT INTO flights_by_datetime (flight_id, datetime, pk) VALUES (%s, %s, %s)',
                        (flight_id, dt, hangar_id))

        return flight_id

    def approve(self, whom_approved: str, approval_status: int = APROV_APPROVED) -> None:
        with Cassandra() as cur:
            cur.execute('UPDATE flights SET whom_approved=%s, approved=%s WHERE flight_id=%s',
                        (whom_approved, approval_status, self.flight_id))
        self.whom_approved = whom_approved
        self.approved = approval_status

    def get_all(self) -> tp.Iterator['Flight']:
        with Cassandra() as cur:
            p = cur.execute('SELECT flight_id FROM flights')
        for flight_id, in p:
            yield Flight(flight_id)

    def __init__(self, flight_id: tp.Union['Flight', str]):
        if isinstance(flight_id, Flight):
            flight_id = flight_id.flight_id
        self.flight_id: str = flight_id
        self.datetime: int = None
        self.drone_id: str = None
        self.hangar_id: str = None
        self.whom_approved: str = None
        self.approved: int = None
        self.user_id: str = None
        self.type_of_mission: int = None
        self._status: int = None
        self._drone: Device = None
        self._hangar: Device = None
        self.datafile_ids: tp.Set[str] = set()
        self._stream_port: int = None
        self.refresh()

    @property
    def stream_port(self) -> int:
        return self._stream_port

    @stream_port.setter
    def stream_port(self, value: int):
        with Cassandra() as cur:
            cur.execute('UPDATE flights SET stream_port=%s WHERE flight_id=%s', (value, self.flight_id))
        self._stream_port = value

    @property
    def status(self) -> int:
        return self._status

    @status.setter
    def status(self, new_status: int) -> None:
        with Cassandra() as cur:
            cur.execute('UPDATE flights SET status=%s WHERE flight_id=%s', (new_status, self.flight_id))
        self._status = new_status

    def set_whom_approved(self, whom_approved: str) -> None:
        self.whom_approved = whom_approved
        with Cassandra() as cur:
            cur.execute('UPDATE flights SET whom_approved=%s WHERE flight_id=%s', (whom_approved, self.flight_id))

    def to_json(self) -> dict:
        return {
            'flight_id': self.flight_id,
            'datetime': self.datetime,
            'drone_id': self.drone_id,
            'hangar_id': self.hangar_id,
            'user_id': self.user_id,
            'status': self.status,
            'whom_approved': self.whom_approved,
            'approved': self.approved,
            'type_of_mission': self.type_of_mission
        }


class DailyScheduled(JSONAble):
    class DoesNotExist(DoesNotExist):
        pass

    def __str__(self) -> str:
        return '<DailyScheduled %s drone=%s hangar=%s>' % (self.seconds, self.drone_id, self.hangar_id)

    def __init__(self, seconds: int):
        self.seconds: int = seconds
        with Cassandra as cur:
            p = cur.execute('SELECT drone_id, hangar_id, user_id FROM daily_scheduled WHERE pk=%s AND seconds=%s',
                            ('a', seconds))

        if not p:
            raise DailyScheduled.DoesNotExist(seconds)

        self.drone_id: str = p[0][0]
        self.hangar_id: str = p[0][1]
        self.user_id: str = p[0][2]

    def delete(self) -> None:
        with Cassandra() as cur:
            cur.execute('DELETE FROM daily_scheduled WHERE pk=%s AND seconds=%s', ('a', self.seconds))

    @staticmethod
    def delete_all() -> None:
        with Cassandra() as cur:
            cur.execute('DELETE FROM daily_scheduled WHERE pk=%s', ('a',))

    @staticmethod
    def create(seconds: int, drone_id: str, hangar_id: str, user_id: str) -> None:
        with Cassandra() as cur:
            cur.execute(
                'INSERT INTO daily_scheduled (pk, drone_id, hangar_id, seconds, user_id) VALUES (%s, %s, %s, %s, %s)',
                ('a', drone_id, hangar_id, seconds, user_id))

    def to_json(self) -> dict:
        return {
            'seconds': self.seconds,
            'drone_id': self.drone_id,
            'hangar_id': self.hangar_id,
            'user_id': self.user_id
        }

    @staticmethod
    def get_all_schedules() -> tp.List['DailyScheduled']:
        with Cassandra() as cur:
            p = cur.execute('SELECT seconds FROM daily_scheduled WHERE pk=%s ORDER BY seconds ASC', ('a',))
        return [DailyScheduled(a) for a, in p]

    def create_flight_for(self, base_day: int) -> str:
        """
        :param base_day: Timestamp of 00:00 for the day that this flight is to be created
        :return: an UUID of the flight just created
        """
        return Flight.create(self.seconds + base_day, self.drone_id, self.hangar_id, self.user_id, Flight.TOM_AUTOMATIC)


class DataFileAttached:
    LOG = 0  # log file_type
    ALARM = 1  # log file_type

    class DoesNotExist(DoesNotExist):
        pass

    def __init__(self, datafile_id: str):
        self.datafile_id = datafile_id

        with Cassandra() as cur:
            p = cur.execute('SELECT flight_id, file_type, content FROM datafiles WHERE datafile_id=%s',
                            (datafile_id,))

        if not p:
            raise DataFileAttached.DoesNotExist(datafile_id)

        self.flight_id, self.file_type, self.content = p[0]

    @staticmethod
    def create(flight: Flight, file_type: int,
               content: tp.Union[bytes, bytearray, tp.BinaryIO]) -> str:
        datafile_id = uuid.uuid4().hex
        flight_id = flight.flight_id

        assert file_type in (DataFileAttached.ALARM, DataFileAttached.LOG)

        if isinstance(content, str):
            raise TypeError("Data file content must be binary")
        elif isinstance(content, (bytes, bytearray)):
            content = bytearray(content)
        else:
            content = content.read()

        with Cassandra.batch() as cur:
            cur.execute("INSERT INTO datafiles (datafile_id, file_type, flight_id, content) "
                        "VALUES (%s, %s, %s, %s)",
                        (datafile_id, file_type, flight_id, content))
            cur.execute("UPDATE flights SET datafile_ids = datafile_ids + {%s} WHERE flight_id=%s",
                        (datafile_id, flight_id))

        return datafile_id

    def delete(self) -> None:
        with Cassandra.batch() as cur:
            cur.execute('DELETE FROM datafiles WHERE datafile_id=%s', (self.datafile_id,))
            cur.execute('UPDATE flights SET datafile_ids = datafile_ids - {%s} WHERE flight_id=%s', (
                self.datafile_id, self.flight_id
            ))
