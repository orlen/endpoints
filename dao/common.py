import abc
import logging

__all__ = ['JSONable']
logger = logging.getLogger(__name__)


class JSONable:
    """
    A class that allows to convert itself to a JSON-able dictionary
    """

    @abc.abstractmethod
    def to_json(self) -> dict:
        pass
