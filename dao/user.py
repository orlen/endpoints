import hashlib
import logging
import random
import string
import typing as tp

from endpoints.cassandra import Cassandra
from .exceptions import DoesNotExist

__all__ = ['User']
logger = logging.getLogger(__name__)


def get_hash_salt_pair(new_password: str) -> tp.Tuple[str, str]:
    new_salt = ''.join((random.choice(string.ascii_letters + string.digits) for x in range(10)))
    new_pwd_hash = hashlib.sha256((new_password + new_salt).encode('utf8')).hexdigest()
    return new_pwd_hash, new_salt


class User:
    class DoesNotExist(DoesNotExist):
        pass

    class AlreadyExists(Exception):
        pass

    def __init__(self, user_id: str):
        self.user_id = user_id

        with Cassandra() as cur:
            p = cur.execute('SELECT password_hash, password_salt FROM users WHERE user_id=%s', (user_id,))

        if not p:
            raise User.DoesNotExist("User %s does not exist" % (user_id,))

        self.password_hash, self.password_salt = p[0]

    def check_password(self, password: str) -> bool:
        return self.password_hash == hashlib.sha256((password + self.password_salt).encode('utf8')).hexdigest()

    def set_password(self, new_password: str) -> None:
        new_pwd_hash, new_salt = get_hash_salt_pair(new_password)
        with Cassandra() as cur:
            cur.execute('UPDATE users SET password_hash=%s, password_salt=%s WHERE user_id=%s', (
                new_pwd_hash, new_salt, self.user_id
            ))

    @staticmethod
    def create(user_id: str, password: str) -> str:
        try:
            User(user_id)
        except User.DoesNotExist:
            pass
        else:
            raise User.AlreadyExists(user_id)

        new_pwd_hash, new_salt = get_hash_salt_pair(password)
        with Cassandra() as cur:
            cur.execute('INSERT INTO users (user_id, password_hash, password_salt) VALUES (%s, %s, %s)', (
                user_id, new_pwd_hash, new_salt
            ))

        return user_id

    def delete(self) -> None:
        with Cassandra() as cur:
            cur.execute('DELETE FROM users WHERE user_id=%s', (self.user_id,))
