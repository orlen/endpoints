import logging
import typing as tp 
import uuid

from endpoints.cassandra import Cassandra
from endpoints.redis import redis
from .exceptions import DoesNotExist

logger = logging.getLogger(__name__)

__all__ = ['Device', 'DeviceDataFileAttached']


class Device:
    class DoesNotExist(DoesNotExist):
        pass

    def refresh(self) -> None:
        with Cassandra() as cur:
            p = cur.execute('SELECT crc32, auth_key, device_datafile_ids, device_type, ip FROM devices WHERE device_id=%s',
                            (self.device_id,))
        if not p:
            raise Device.DoesNotExist(self.device_id)

        self.crc32, self.auth_key, self.device_datafile_ids, self.device_type, self._ip = p[0]
        if self.device_datafile_ids is None:
            self.device_datafile_ids = set()

    @property
    def ip(self) -> str:
        if self._ip is None:
            self.refresh()
        return self._ip

    @ip.setter
    def ip(self, value: str) -> None:
        with Cassandra() as cur:
            cur.execute('UPDATE devices SET ip=%s WHERE device_id=%s', (value, self.device_id))

        self._ip = value

    @staticmethod
    def get_all_devices_of_type(device_type: str) -> tp.List['Device']:
        with Cassandra() as cur:
            p = cur.execute('SELECT device_id, device_type FROM devices')

        return [Device(device_id) for device_id, device_type_ in p if device_type_ == device_type]

    def delete(self) -> None:
        logger.warning("Deleting a device %s", self.device_id)
        self.refresh()
        for device_datafile_id in self.device_datafile_ids:
            d = DeviceDataFileAttached(device_datafile_id)
            d.delete()

        with Cassandra() as cur:
            cur.execute('DELETE FROM devices WHERE device_id=%s', (self.device_id,))

    @staticmethod
    def create(device_id: str, crc32: str, auth_key: str, device_type: str) -> str:
        assert device_type in ('drone', 'hangar')
        logger.warning("Creating new device %s", device_id)
        with Cassandra() as cur:
            cur.execute('INSERT INTO devices (device_id, crc32, auth_key, device_type) VALUES (%s, %s, %s, %s)',
                        (device_id, crc32, auth_key, device_type))
        return device_id

    def __init__(self, device_id: tp.Union[str, 'Device']):
        if isinstance(device_id, Device):
            device_id = device_id.device_id
        self.device_id: str = device_id
        self.crc32: str = None
        self.device_type: str = None
        self.auth_key: str = None
        self.device_datafile_ids: tp.Set[str] = set()
        self._ip: str = None
        self.refresh()

    @property
    def state(self) -> str:
        return redis.plain_get('%s.%s.STATE' % (self.device_type.upper(), self.device_id))

    @state.setter
    def state(self, value: str):
        redis.plain_set('%s.%s.STATE' % (self.device_type.upper(), self.device_id), value)


class DeviceDataFileAttached:
    LOG = 0
    ALARM = 1

    class DoesNotExist(Exception):
        pass

    def __init__(self, datafile_id: str):
        self.datafile_id: str = datafile_id

        with Cassandra() as cur:
            p = cur.execute('SELECT device_id, file_type, content FROM device_datafiles WHERE datafile_id=%s',
                            (self.datafile_id,))

        if not p:
            raise DeviceDataFileAttached.DoesNotExist(datafile_id)

        self.device_id: str = p[0][0]
        self.file_type: int = p[0][1]
        self.content: bytes = p[0][2]

    @staticmethod
    def create(device: Device, file_type: int,
               content: tp.Union[bytes, bytearray, tp.BinaryIO]) -> str:
        datafile_id: str = uuid.uuid4().hex
        device_id: str = device.device_id

        assert file_type in (DeviceDataFileAttached.ALARM, DeviceDataFileAttached.LOG)

        if isinstance(content, str):
            raise TypeError("Data file content must be binary")
        elif isinstance(content, (bytes, bytearray)):
            content = bytearray(content)
        else:
            content = content.read()

        with Cassandra.batch() as cur:
            cur.execute("INSERT INTO device_datafiles (datafile_id, file_type, device_id, content) "
                        "VALUES (%s, %s, %s, %s)",
                        (datafile_id, file_type, device_id, content))
            cur.execute("UPDATE devices SET device_datafile_ids = device_datafile_ids + {%s} WHERE device_id=%s",
                        (datafile_id, device_id))

        return datafile_id

    def delete(self) -> None:
        with Cassandra.batch() as cur:
            cur.execute('DELETE FROM device_datafiles WHERE datafile_id=%s', (self.datafile_id,))
            cur.execute('UPDATE devices SET device_datafile_ids = device_datafile_ids - {%s} WHERE device_id=%s', (
                self.datafile_id, self.device_id
            ))
