import logging
import time
import typing as tp
import json

from endpoints.cassandra import Cassandra
from .exceptions import DoesNotExist

__all__ = ['Failure']
logger = logging.getLogger(__name__)


class Failure:
    class DoesNotExist(DoesNotExist):
        pass

    def __init__(self, user_id: str, datetime: int):
        self.user_id: str = user_id
        self.datetime: int = datetime

        with Cassandra() as cur:
            p = cur.execute('SELECT failure, reason FROM failures WHERE user_id=%s AND datetime=%s', (user_id, datetime))

        if not p:
            raise Failure.DoesNotExist()

        self.failure: str = p[0][0]
        self.reason: tp.Optional[dict] = None if p[0][1] is None else json.loads(p[0][1])

    @staticmethod
    def get_all_failures(user_id: str) -> tp.List['Failure']:
        with Cassandra() as cur:
            p = cur.execute('SELECT datetime FROM failures WHERE user_id=%s ORDER BY datetime DESC', (user_id,))

        return [Failure(user_id, f[0]) for f in p]

    @staticmethod
    def create(user_id: str, failure: str, reason: dict = None) -> int:
        datetime: int = int(time.time())
        if reason is not None:
            reason = json.dumps(reason)

        with Cassandra() as cur:
            cur.execute('INSERT INTO failures (user_id, datetime, failure, reason) VALUES (%s, %s, %s, %s) USING TTL 86400',
                        (user_id, datetime, failure, reason))

        return datetime

    def delete(self):
        with Cassandra() as cur:
            cur.execute('DELETE FROM failures WHERE user_id=%s AND datetime=%s', (self.user_id, self.datetime))

    def to_json(self) -> dict:
        return {
            'user_id': self.user_id,
            'failure': self.failure,
            'datetime': self.datetime,
            'reason': self.reason
        }
