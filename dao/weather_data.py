import logging
import time

from endpoints.cassandra import Cassandra

logger = logging.getLogger(__name__)


class WeatherDataPoint:
    @staticmethod
    def create(weather_attribute: str, value: float, timestamp: int = None) -> 'WeatherDataPoint':
        timestamp = int(timestamp or time.time())
        with Cassandra() as cur:
            cur.execute('INSERT INTO weather_data (weather_attribute, timestamp, value) VALUES (%s, %s, %s)',
                        (weather_attribute, timestamp, value))

        return WeatherDataPoint(weather_attribute, timestamp, value)

    def __init__(self, weather_attribute: str, timestamp: int, value: float):
        self.weather_attribute: str = weather_attribute
        self.timestamp: int = timestamp
        self.value: float = value
