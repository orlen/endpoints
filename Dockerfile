FROM smokserwis/build:jdk8 AS weather-java

ADD weather/GetSnap.java /GetSnap.java
RUN javac /GetSnap.java

FROM python:3.6

# Install ffmpeg and ffserver
RUN apt-get update && \
    apt-get install -y --no-install-recommends libx264-dev x264 \
                                               libx264-155 \
                                               libtheora-dev \
                                               libtheora-bin \
                                               ffmpeg2theora \
                                               wget \
                                               nasm && \
    apt-get clean

RUN wget https://ffmpeg.org/releases/ffmpeg-3.4.6.tar.gz && \
    tar xvf ffmpeg-3.4.6.tar.gz && \
    cd ffmpeg-3.4.6 && \
    ./configure --enable-libtheora --enable-libx264 --enable-gpl && \
    make && \
    make install && \
    cd .. && \
    rm -rf ffmpeg-3.4.*


# requirements.txt is copied first to make use of Docker caching

COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

RUN apt-get update && \
    apt-get install -y openjdk-11-jre libssl1.1 && \
    apt-get clean

# Required context: root of the project
ENV CLASSPATH="/app/weather/PIJDBCDriver.jar:." \
    PI_RDSA_LIB64="/app/weather/libRdsaWrapper64-1.5b.so"

ENV PI_DATA_HOST="BPLAPIINT2" \
    PI_DATA_ARCH="BPLAPITI1" \
    PI_DATA_USER="sscervi_pi" \
    PI_DATA_PASS="nrb^!DeH#[DSDy" \
    PI_DATA_PORT="5461"
COPY --from=weather-java /GetSnap.class /app/GetSnap.class
ENV LD_LIBRARY_PATH=/app/weather

COPY . /app
WORKDIR /app

# Configure weather provider
RUN chmod ugo+x /app/weather/*.so.1.0.0

VOLUME /videos

CMD ["/usr/local/bin/python3.6", "-m", "endpoints.app"]
STOPSIGNAL SIGKILL
EXPOSE 80
EXPOSE 8090

