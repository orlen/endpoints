import json
import logging
import typing as tp

import audit
from dao import SocketClosed
from satella.json import JSONAble

logger = logging.getLogger(__name__)

"""
A set of common functions used throughout the module
"""


class SocketWrapper:
    """
    A socket wrapper that allows sending JSON-able classes or dictionaries.
    It also handles audit issues.

    Otherwise if an unfound method (except send() and receive()) is found,
    it is routed to the socket wrapped.
    """

    def __init__(self, socket_to_wrap, logname: str, enable_auditing: bool = True):
        self.socket_to_wrap = socket_to_wrap
        self.logname: str = logname
        self.enable_auditing = enable_auditing

    def send(self, frame: tp.Union[dict, JSONAble]) -> None:
        """
        :raises SocketClosed: upon sending to a closed socket
        """
        if self.socket_to_wrap.closed:
            raise SocketClosed()

        if isinstance(frame, (str, bytes)):
            raise ValueError('Sending simple strings and bytes is invalid')
        elif isinstance(frame, dict):
            pass
        else:
            frame = frame.to_json()
        if self.enable_auditing:
            audit.send(self.logname, frame)

        self.socket_to_wrap.send(json.dumps(frame))

    def receive(self) -> tp.Union[None, dict]:
        """
        Return None on connection close, else unserialized JSON dict
        :raises SocketClosed: on socket closed
        """
        msg = self.socket_to_wrap.receive()
        if msg is None:
            raise SocketClosed()
        if isinstance(msg, bytes):
            raise ValueError('Unexpected bytes reply!')
        else:
            frame = json.loads(msg)

        if self.enable_auditing:
            audit.recv(frame)

        return frame

    def __getattr__(self, item: str) -> tp.Any:
        return getattr(self.socket_to_wrap, item)
