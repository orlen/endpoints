import java.io.Console;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class GetSnap {

    public static void main(String[] args) {

        int argsLength = args.length;
        int portNumberArgPosition = 5;
        Console console = System.console();
        String dasName = System.getenv("PI_DATA_HOST");
        String dataSourceName = System.getenv("PI_DATA_ARCH");
        String enableCertificateValidation = "No";
        String isTrustedConnection = "No";
        String useDCA = "No";
        String userName = System.getenv("PI_DATA_USER");
        String password = System.getenv("PI_DATA_PASS");
        String portNumber = System.getenv("PI_DATA_PORT");
        String logLevel = "0";
        String tagNamePattern = args[0];


        /*------------------------ Get data using PI JDBC driver ------------------------*/
        Connection connection = null;
        String url = "";
        String driverClassName = "com.osisoft.jdbc.Driver";
        PreparedStatement pStatement = null;
        ResultSet resultSet = null;
        Properties properties = new Properties();

        url = "jdbc:pioledb://" + dasName + "/Data Source=" + dataSourceName + "; Integrated Security=SSPI";

        properties.put("EnableCertificateValidation", enableCertificateValidation);
        properties.put("TrustedConnection", isTrustedConnection);
        if (isTrustedConnection.equals("No") && useDCA.equals("No")) {
            properties.put("user", userName);
            properties.put("password", password);
        }
        properties.put("Port", portNumber);
        properties.put("LogConsole", "True");
        properties.put("LogLevel", logLevel);

        try {
            Class.forName(driverClassName).newInstance();
            connection = DriverManager.getConnection(url, properties);

            pStatement = connection.prepareStatement("SELECT tag, value FROM pisnapshot WHERE tag like ?");

            DatabaseMetaData metaData = connection.getMetaData();
            // Bind parameter representing the PI tag name
            pStatement.setString(1, tagNamePattern);
            resultSet = pStatement.executeQuery();

            // Read the data
            while (resultSet.next()) {
                String value, tag;
                tag = resultSet.getString(1);
                value = resultSet.getString(2);
                System.out.println(value);
            }
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
        finally {
            try {
                if (resultSet != null)
                    resultSet.close();
                if (pStatement != null)
                    pStatement.close();
                if (connection != null)
                    connection.close();
            }
            catch (SQLException ex) {
                System.err.println(ex);
            }
        }
        /*------------------------ Get data using PI JDBC driver end------------------------*/
    }

    private static boolean isStringFalseOrNo(String argument) {
        return argument.toLowerCase().startsWith("f") || argument.toLowerCase().startsWith("n");
    }
}