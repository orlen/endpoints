import logging
import subprocess
import time

from flask import Blueprint
from satella.coding.structures import Singleton
from satella.json import JSONAble
from dao import Session
from dao import WeatherDataPoint
from endpoints.decorators import cron, as_json, must_be_logged
from endpoints.settings import DEBUG

logger = logging.getLogger(__name__)

weather_blueprint = Blueprint('Weather', __name__)


@Singleton
class WeatherInfo(JSONAble):
    def __init__(self):
        self.cisn: float = 0.0
        self.kier_wiatr: float = 0.0
        self.predkosc_wiatr: float = 0.0
        self.temp_pv: float = 0.0
        self.wilg_pv: float = 0.0
        self.opad: float = 0.0
        self.indeks_kp: float = 0.0

    def to_json(self) -> dict:
        return {
            'cisn': self.cisn,
            'kier_wiatr': self.kier_wiatr,
            'predkosc_wiatr': self.predkosc_wiatr,
            'temp_pv': self.temp_pv,
            'wilg_pv': self.wilg_pv,
            'opad': self.opad,
            'indeks_kp': self.indeks_kp
        }


MAPPINGS = {
    'ZSP.CISN_.PV': 'cisn',
    'ZSP.KIER_.PV': 'kier_wiatr',
    'ZSP.PREDK_.PV': 'predkosc_wiatr',
    'ZSP.TEMP_.PV': 'temp_pv',
    'ZSP.WILG_.PV': 'wilg_pv',
    'TRZ.OPAD.PV': 'opad'
}


@weather_blueprint.route('/v1/weather', methods=['GET', 'POST'])
@as_json
@must_be_logged
def get_weather_info(session: Session) -> WeatherInfo:
    """
    Get current weather info
    ---
    security:
        - auth
    tags:
        - user
    responses:
        '200':
            description: Here's your weather info
            content:
                application/json:
                    schema:
                        $ref: '#/components/schemas/Weather'
        '403':
            description: Unauthorized
    """
    return WeatherInfo()


@cron(60.0)
def update_weather_info() -> None:
    if DEBUG:
        return

    weather_info = WeatherInfo()
    for key, value in MAPPINGS.items():
        output = subprocess.Popen('java GetSnap ' + key, shell=True, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
        time.sleep(5)
        if output.poll() is None:
            time.sleep(5)
            output.terminate()
            time.sleep(1)
        output.poll()
        return_value = output.stdout.read(2048).decode('utf8').split('\n')[0]
        try:
            output = float(return_value)
        except ValueError:
            logger.error('Failed processing %s with output of %s', key, output)
        else:
            weather_info.__setattr__(value, output)
            WeatherDataPoint.create(key, output)
