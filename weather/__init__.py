import logging
from .weather import weather_blueprint
logger = logging.getLogger(__name__)

blueprints = [weather_blueprint]
