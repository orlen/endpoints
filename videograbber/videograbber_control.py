import signal
import os
import subprocess
import typing as tp
from collections import deque
from satella.coding.structures import Singleton
from endpoints.kvs import KVS
from endpoints.settings import VIDEO_PATH
from dao import Flight

SOURCE_CONFIG_FILE = '/app/videograbber/rtsp_restream.conf'

import time
import os
import logging

logger = logging.getLogger(__name__)

@Singleton
class VideoGrabberControls:
    """
    A class that controls video grabbing for given flights
    """

    def __init__(self):
        self.processes_for_flight: tp.Dict[str, tp.Tuple[subprocess.Popen, subprocess.Popen]] = {}
        self.ports_assigned: tp.Dict[str, int] = {}
        self.ports_available: tp.List[int] = deque([8090])
        self.config_file_for: tp.Dict[str, str] = {}

    def start_for_flight(self, flight_id: str) -> int:
        """
        Returns a port that the video is streamed at. Access it using http://address:port/stream.ogg
        :raise IndexError: all ports have been exhausted
        """
        if flight_id in self.processes_for_flight:
            return
        drone_video_source = 'rtsp://%s:554/live' % (Flight(flight_id).drone.ip, )

        port_to_use = self.ports_available.pop()
        self.ports_assigned[flight_id] = port_to_use

        self.config_file_for[flight_id] = '/tmp/rtsp_restream.%s.conf' % (port_to_use,)

        with open(SOURCE_CONFIG_FILE, 'r') as fin:
            data = fin.read().replace('PORT_TO_REPLACE', str(port_to_use))
        with open(self.config_file_for[flight_id], 'w') as fout:
            fout.write(data)

        ffmpeg_server_url = 'http://127.0.0.1:%s/cam1.%s.ffm' % (port_to_use, port_to_use)
        video_save_path = os.path.join(VIDEO_PATH, '%s.mp4' % (flight_id,))
        logger.warning('Connecting to %s', drone_video_source)

        ffserver = subprocess.Popen(['/usr/local/bin/ffserver', '-f', self.config_file_for[flight_id]],
                                    stdout=subprocess.DEVNULL,
                                    stderr=subprocess.DEVNULL)
        time.sleep(2)
        ffmpeg = subprocess.Popen(
            ['/usr/local/bin/ffmpeg', '-rtsp_transport', 'tcp', '-i', drone_video_source, '-vcodec',
             'libtheora', ffmpeg_server_url, '-f', 'segment', '-strftime', '1', '-segment_time',
             '9999999', '-segment_format', 'mp4', video_save_path],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL)
        time.sleep(3)
        self.processes_for_flight[flight_id] = ffmpeg, ffserver

        if ffmpeg.poll() is not None:
            logger.error('ffmpeg aborted with result code of %s', ffmpeg.poll())

        if ffserver.poll() is not None:
            logger.error('ffserver aborted with result code of %s', ffserver.poll())

    def stop_flight(self, flight_id: str):
        try:
            ffmpeg, ffserver = self.processes_for_flight.pop(flight_id)
        except KeyError:
            return
        ffserver.send_signal(signal.SIGINT)

        ffmpeg.send_signal(signal.SIGINT)

        port_assigned = self.ports_assigned.pop(flight_id)

        started_at = time.monotonic()
        max_wait_for: float = 10.0
        while (ffserver.poll() is None or ffmpeg.poll() is None) and (time.monotonic() - started_at < max_wait_for):
            time.sleep(0.5)

        if ffserver.poll() is None:
            logger.warning('Forcibly killing ffserver at port %s', port_assigned)
            ffserver.kill()

        if ffmpeg.poll() is None:
            logger.warning('Forcibly killing ffmpeg at port %s', port_assigned)
            ffmpeg.kill()

        self.ports_available.append(port_assigned)
        os.unlink(self.config_file_for.pop(flight_id))
