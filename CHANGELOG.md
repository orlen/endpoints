The principal change log is kept at
[Cervi GitLab](https://git.cervirobotics.com/dronhub/orlen-backend/endpoints/-/tags).

This file serves only to track the changes for current release.

# v0.4.1

* made tests mandatory again
* more Swagger docs
* added an endpoint to obtain audit logs
* logins will be now done using USING TTL

