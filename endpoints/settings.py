import logging
import os

logger = logging.getLogger(__name__)

DEBUG = eval(os.environ.get('DEBUG', 'True'))
VIDEO_PATH = os.environ.get('VIDEO_SAVE_PATH', '/videos')
