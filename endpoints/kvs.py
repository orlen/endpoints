import logging
import typing

from endpoints.cassandra import Cassandra

logger = logging.getLogger(__name__)

__all__ = ['KVS']


class CassandraKVS:
    def get(self, key: str, default: str) -> str:
        try:
            return self[key]
        except KeyError:
            return default

    def update(self, mapping: dict, **kwargs) -> None:
        for key, value in mapping.items():
            self[key] = value
        for key, value in kwargs.items():
            self[key] = value

    def __contains__(self, key: str) -> bool:
        try:
            self[key]
        except KeyError:
            return False
        else:
            return True

    def __getitem__(self, item: str) -> str:
        with Cassandra as cur:
            p = cur.execute('SELECT value FROM kvs WHERE key=%s', (item,))
        if not p:
            raise KeyError('Key %s not found' % (item,))
        return p[0][0]

    def __setitem__(self, key: str, value: typing.Any):
        with Cassandra as cur:
            cur.execute('INSERT INTO kvs (key, value) VALUES (%s, %s)', (key, str(value)))

    def __delitem__(self, key: str):
        with Cassandra as cur:
            cur.execute('DELETE FROM kvs WHERE key=%s', (key,))


KVS = CassandraKVS()
