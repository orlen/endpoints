import functools
import logging
import threading
import time

from flask import request, abort, Response
from satella.instrumentation import Traceback

from audit import audit, AUDIT_SECURITY
from dao import Session, User
from satella.json import json_encode

logger = logging.getLogger(__name__)

__all__ = ['must_be_logged', 'as_json', 'cron', 'silence_and_log_exceptions']


def silence_and_log_exceptions(f):
    @functools.wraps(f)
    def inner(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except BaseException as e:
            try:
                tb = Traceback()
                logger.error('Exception during processing %s of type %s', f, tb.pretty_format())
            except BaseException as e:
                logger.error('Exception during processing %s of content %s', f, repr(e))
    return inner


def cron(seconds):
    def inner(f):
        class Thread(threading.Thread):
            def __init__(self, f):
                super(Thread, self).__init__()
                self.f = silence_and_log_exceptions(f)
                self.daemon = True

            def run(self):
                while True:
                    self.f()
                    time.sleep(seconds)

        Thread(f).start()

        return f

    return inner


def as_json(f):
    """
    Transform the output of a view into a JSON.

    This will correctly handle objects, that are part of the conversion,
    and properly subclass dao.JSONable
    :param f:
    :return:
    """
    @functools.wraps(f)
    def as_json_inner(*args, **kwargs):

        result = f(*args, **kwargs)

        if isinstance(result, tuple):
            if len(result) == 2:
                result, code = result
                if isinstance(code, int):
                    return Response(json_encode(result), status=code)

        if isinstance(result, Response):
            result.set_data(json_encode(result.response))
            return result
        elif result is None:
            return Response()
        else:
            return Response(json_encode(result))

    return as_json_inner


SESSION_EXTENSION = 15*60   # extend the session by 15 minutes each time there's a sign of life from the user


def must_be_logged(f):
    @functools.wraps(f)
    def must_be_logged_inner(*args, **kwargs):

        class UnauthorizedException(Exception):
            def __init__(self, message: str):
                super(UnauthorizedException, self).__init__()
                self.message: str = message

        try:
            if 'session_id' not in request.cookies:
                raise UnauthorizedException('session_id_not_given')

            try:
                session = Session(request.cookies['session_id'])
            except Session.DoesNotExist:
                raise UnauthorizedException('session_not_found')

            if session.expiry_time <= time.time():
                session.delete()
                raise UnauthorizedException('session_expired')

            try:
                session.user
            except User.DoesNotExist:
                session.delete()
                raise UnauthorizedException('user_deleted')

            session.extend_for(extend_to=time.time()+SESSION_EXTENSION)

            return f(session, *args, **kwargs)
        except UnauthorizedException as e:
            data = {
                'reason': e.message,
                'what_happened': 'bad_session',
                'url': request.url,
                'method': request.method,
                'ip': request.headers.get('X-Forwarded-For', '')
            }
            try:
                data['login'] = session.user_id
            except NameError:
                data['login'] = None

            audit(AUDIT_SECURITY, data)
            abort(401)

    return must_be_logged_inner
