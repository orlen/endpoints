import gevent.monkey

# First of all, monkey-patch everything to gevent
import yaml
from flasgger import Swagger

gevent.monkey.patch_all()

import logging
import typing
import random
import threading
from flask import Flask
from flask_json import FlaskJSON, as_json
from flask_sockets import Sockets
from endpoints import VERSION, BUILD
from flask_cors import CORS
from gevent import pywsgi
from geventwebsocket.handler import WebSocketHandler

from .settings import DEBUG

__all__ = ['run_application']

logger = logging.getLogger(__name__)

app = Flask(__name__)
app.config['SECRET'] = 'hejczytywiesz'
app.config['JSON_ADD_STATUS'] = False
app.config['UPLOAD_FOLDER'] = '/app/upload_files'
websockets = Sockets(app)
websockets.init_app(app)
appjson = FlaskJSON(app)
appjson.init_app(app)

if DEBUG:
    CORS(app, supports_credentials=True)


def register_blueprint(module) -> None:
    for blueprint in module.blueprints:
        (websockets if blueprint.name.endswith(' WS') else app).register_blueprint(blueprint)


# import all handlers
import user
import devices
import weather

register_blueprint(user)
register_blueprint(devices)
register_blueprint(weather)


with open('/app/app-template.yml', 'r') as f_in:
    template = yaml.load(f_in, Loader=yaml.Loader)
template['info']['version'] = VERSION
app.config['SWAGGER'] = {'openapi': '3.0.2'}
Swagger(app, template=template)
del template


@app.route("/v1/version", methods=['GET', 'POST'])
@as_json
def version() -> dict:
    return {'version': VERSION,
            'build': BUILD}


def run_application(logging_level: typing.Union[int, None] = None) -> "never returns":
    if logging_level is not None:
        logging.basicConfig(level=logging_level)

    def define_timer(first_time=False) -> None:
        a = 60
        if first_time:
            a += random.randint(0, 30)  # in order to lessen the impact of instances starting up at the same time
        t = threading.Timer(a, do_timer)
        t.daemon = True
        t.start()

    def do_timer() -> None:
        define_timer()
        app.test_client().get('/v1/cron')  # do this internally, not over the network

    define_timer(first_time=True)

    server = pywsgi.WSGIServer(('0.0.0.0', 80), app, handler_class=WebSocketHandler)
    logger.warning("Endpoints app v%s build %s is starting now", VERSION, BUILD)
    server.serve_forever()


if __name__ == '__main__':
    run_application(logging.INFO if DEBUG else logging.INFO)
