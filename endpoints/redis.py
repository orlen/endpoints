import os
import types
import typing as tp

from redis import Redis

__all__ = ['redis']

REDIS_HOST = os.environ.get('REDIS_HOST', 'redis')
REDIS_PORT = int(os.environ.get('REDIS_PORT', '6379'))
REDIS_DB = int(os.environ.get('REDIS_DB', '0'))

redis = Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)


def redis_plain_get(self, key: str, default: tp.Optional[str] = None, stfu: bool = False) -> str:
    """
    Just plain out get a value from redis

    :param key: Key to read
    :param default: default value to return when the key does not exist
    :param stfu: if stfu is True and default is not given, a None will
        be returned instead of KeyError being raised on the key being absent
    :raises KeyError: upon key not being found
    """
    v = self.get(key)
    if v is None:
        if default is None:
            if stfu:
                return None
            else:
                raise KeyError(key)
        else:
            return default
    return v.decode('utf8')


def redis_plain_set(self, key: str, value: str) -> None:
    value = value.encode('utf8')
    self.set(key, value)


redis.plain_get = types.MethodType(redis_plain_get, redis)
redis.plain_set = types.MethodType(redis_plain_set, redis)
