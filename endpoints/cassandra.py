import os

import cassandra.auth
import cassandra.cluster
import cassandra.policies
import cassandra.query

import cassandra

__all__ = ['Cassandra']


class CassandraBatchInsertor:
    def __init__(self, cursor, consistency_level=None, retry_policy=None):
        self.cursor = cursor
        self.consistency_level = consistency_level
        self.retry_policy = retry_policy

    def execute(self, *args, **kwargs):
        self.batch.add(*args, **kwargs)

    def __enter__(self):
        self.batch = cassandra.query.BatchStatement(consistency_level=self.consistency_level,
                                                    retry_policy=self.retry_policy,
                                                    session=self.cursor)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cursor.execute(self.batch)


class CassandraCluster:
    def __init__(self):
        self.cluster = cassandra.cluster.Cluster([os.environ.get('CASSANDRA_HOST', 'cassandra')],
                                                 load_balancing_policy=cassandra.cluster.TokenAwarePolicy(
                                                     cassandra.cluster.DCAwareRoundRobinPolicy(
                                                         local_dc=os.environ.get('CASSANDRA_DC', 'datacenter1')
                                                     )
                                                 ),
                                                 auth_provider=cassandra.auth.PlainTextAuthProvider(
                                                     username=os.environ.get('CASSANDRA_USERNAME', 'cassandra'),
                                                     password=os.environ.get('CASSANDRA_PASSWORD', 'cassandra')
                                                 ))
        try:
            self.session = self.cluster.connect(os.environ.get('CASSANDRA_KEYSPACE', 'api'))
        except cassandra.cluster.NoHostAvailable:
            raise RuntimeError('No host for Cassandra; terminating')

    def __enter__(self):
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        return False

    def batch(self, consistency_level=None, retry_policy=None):
        return CassandraBatchInsertor(self.session, consistency_level=consistency_level, retry_policy=retry_policy)

    def __call__(self, *args, **kwargs):
        return self


Cassandra = CassandraCluster()
