from user.dailyschedules import dailyschedules_blueprint
from user.flight import flight_blueprint
from user.login_and_auth import login_and_auth_blueprint
from user.user import user_blueprint
from user.user_ws import user_ws_blueprint

blueprints = [flight_blueprint, login_and_auth_blueprint, user_ws_blueprint, dailyschedules_blueprint, user_blueprint]
