import time

from flask import Blueprint

from common import SocketWrapper
from dao import Session
from devices.globals import SOCKETS_FOR_USERS
from endpoints.redis import redis

user_ws_blueprint = Blueprint('User WS', __name__)


@user_ws_blueprint.route('/v1/user/websocket')
def user_drone(ws):
    ws = SocketWrapper(ws, 'user', enable_auditing=False)

    session_id = ws.receive()['session']

    if session_id is None:
        return

    session = Session(session_id)
    ws.logname = session.user_id
    try:
        flight_id = redis.plain_get('USER.%s.FLIGHT' % (session.user_id,))
    except KeyError:
        ws.send({"flight_id": None})
    else:
        ws.send({'flight_id': flight_id})

    SOCKETS_FOR_USERS.append(session.user_id, ws)

    while not ws.closed:
        # Do not close this web socket prematurely
        time.sleep(10)
