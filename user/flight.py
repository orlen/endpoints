import datetime
import typing as tp
from time import mktime, time

from flask import request, Blueprint, abort

from dao import Flight, Session
from dao.exceptions import DoesNotExist
from devices.flight import FlightAggregate
from endpoints.decorators import must_be_logged, as_json
from endpoints.redis import redis
from audit import audit, AUDIT_SCHEDULES

flight_blueprint = Blueprint('Flight', __name__)


@flight_blueprint.route("/v1/user/stream/", methods=["POST"])
@as_json
@must_be_logged
def get_stream(session: Session) -> dict:
    """
    Return the URL under which a stream for currently flying drone is available
    ---
    security:
        - auth
    tags:
        - user
    responses:
        '200':
            description: Here's your URL
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            stream:
                                type: string
                                description: URL to get the stream from
                                example: "/stream.ogg"
                        required:
                            - stream
        '403':
            description: Unauthorized
        '409':
            description: Flight not in progress yet
    """
    try:
        flight_id = redis.plain_get('USER.%s.FLIGHT' % (session.user_id,))
    except KeyError:
        return {'status': 'flight-not-enabled'}, 409

    try:
        port = Flight(flight_id).stream_port
    except DoesNotExist:
        redis.delete('USER.%s.FLIGHT' % (session.user_id, ))
        return {'status': 'failed'}, 409

    if port is None:
        return {'status': 'port-not-set'}, 409

    return {
        'stream': '/stream.ogg'
    }


@flight_blueprint.route("/v1/flight/for", methods=["POST"])
@as_json
@must_be_logged
def list_all_schedules(session: Session) -> tp.List[Flight]:
    """
    List all missions for given day
    ---
    security:
        - auth
    tags:
        - user
    requestBody:
        content:
            application/json:
                schema:
                    type: object
                    properties:
                        date:
                            type: string
                            format: date
                            description: Date in form YYYY-MM-DD
                    required:
                        - date
    responses:
        '200':
            description: Here go your flights
            content:
                application/json:
                    schema:
                        type: array
                        items:
                            $ref: '#/components/schemas/Flight'
        '403':
            description: Unauthorized
    """
    data: dict = request.get_json(force=True, silent=True) or {}
    q: datetime.datetime = datetime.datetime.strptime(data['date'], "%Y-%m-%d")
    ts = int(mktime(q.utctimetuple()))

    return Flight.choose(ts, ts + 60 * 60 * 24)


@flight_blueprint.route("/v1/flights/outstanding", methods=['POST'])
@as_json
@must_be_logged
def get_outstanding_flights(session: Session) -> dict:
    """
    Check if there is an active flight in progress for given user
    ---
    security:
        - auth
    tags:
        - user
    responses:
        '200':
            description: OK, response follows
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            flights_outstanding:
                                type: boolean
                                description: Is there an active flight for given user?
                        required:
                            - flights_outstanding
        '403':
            description: Unauthorized
    """
    try:
        flight_id = redis.plain_get('USER.%s.FLIGHT' % (session.user_id,))
    except KeyError:
        return {'flights_outstanding': False}

    try:
        return {'flights_outstanding': FlightAggregate(flight_id).is_waiting_for_user_confirmation()}
    except DoesNotExist:
        redis.delete('USER.%s.FLIGHT' % (session.session_id, ))
        return {'flights_outstanding': False}


@flight_blueprint.route("/v1/flight/new", methods=['PUT'])
@as_json
@must_be_logged
def new_flight(session: Session) -> None:
    """
    Create a new flight
    ---
    security:
        - auth
    tags:
        - user
    requestBody:
        content:
            application/json:
                schema:
                    type: object
                    properties:
                        drone_id:
                            type: string
                            description: ID of the drone
                            example: cr-drone-1
                        hangar_id:
                            type: string
                            description: ID of the hangar
                            example: cr-hangar-1
                    required:
                        - drone_id
                        - hangar_id
    responses:
        '200':
            description: Flight created successfully
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            flight_id:
                                type: string
                                format: uuid
                                description: Newly created flight ID
                                example: fcdb0644621941098b3d6dbecf07610d'
                        required:
                            - flight_id
        '403':
            description: Unauthorized
        '400':
            description: Malformed request
    """
    data = request.get_json(force=True, silent=True) or {}
    try:
        flight_id = Flight.create(int(time()), data['drone_id'], data['hangar_id'], session.user_id, Flight.TOM_MANUAL)
    except KeyError:
        return abort(400)

    audit(AUDIT_SCHEDULES, {
        'login': session.user_id,
        'what_happened': 'manually_triggered',
        'flight_id': flight_id
    })
    return {'flight_id': flight_id}

