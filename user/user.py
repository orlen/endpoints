import logging
import typing as tp

from flask import Blueprint, Response, request

from audit import get, audit, AUDIT_SECURITY
from dao import Failure, Flight, Session
from dao.exceptions import DoesNotExist
from devices.flight import FlightAggregate
from endpoints.decorators import must_be_logged, as_json
from endpoints.redis import redis

__all__ = ['must_be_logged', 'user_blueprint']
logger = logging.getLogger(__name__)

user_blueprint = Blueprint('User', __name__)


@user_blueprint.route('/v1/user/commands', methods=['POST'])
@as_json
@must_be_logged
def issue_command(session: Session):
    """
    Issues a command for the device in current flight
    ---
    security:
        - auth
    tags:
        - user
    requestBody:
        required: true
        content:
            application/json:
                schema:
                    type: object
                    properties:
                      order:
                          type: string
                          description: Order to execute
                          enum:
                            - stop
                            - resume
                            - abort
                            - authorize
                            - cancel
                    required:
                        - order
    responses:
        '200':
            description: Command issued OK
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            status:
                                type: boolean
                                description: Whether the order was processed OK
                        required:
                            - status
        '409':
            description: No flight in progress
        '403':
            description: Unauthorized
    """
    data = request.get_json(force=True, silent=True) or {}

    try:
        flight_id = redis.plain_get('USER.%s.FLIGHT' % (session.user_id,))
    except KeyError:
        return Response(status=409)

    try:
        result = FlightAggregate(flight_id).state.on_user_command(data['order'])
    except DoesNotExist:
        redis.delete('USER.%s.FLIGHT' % (session.user_id, ))
        return Response(status=409)

    Flight(flight_id).set_whom_approved(session.user_id)
    return {
        'status': result
    }


@user_blueprint.route('/v1/user/flights_outstanding', methods=['POST'])
@as_json
@must_be_logged
def flights_outstanding(session: Session):
    """
    Checks if there is an existing flight for given user
    ---
    security:
        - auth
    tags:
        - user
    responses:
        '200':
            description: Command issued OK
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                          flights_outstanding:
                              type: string
                              format: uuid
                              description: >
                                Flight ID if flight's in progress, or a boolean (false) if not
                                in progress right now.
                              example: 4ec02a38aedb497795ff5e5ed90dd9ae
                        required:
                            - flights_outstanding
        '409':
            description: No flight in progress
        '403':
            description: Unauthorized
    """
    try:
        flight_id = redis.plain_get('USER.%s.FLIGHT' % (session.user_id,))
    except KeyError:
        return {"flights_outstanding": False}

    return {'flights_outstanding': FlightAggregate(flight_id).is_waiting_for_user_confirmation()}


@user_blueprint.route('/v1/user/audit/<string:logname>/<int:from_ts>/<int:to_ts>', methods=['GET'])
@as_json
@must_be_logged
def get_audit(session: Session, logname: str, from_ts: int, to_ts: int) -> tp.List[Failure]:
    """
    Enumerate all audit events
    ---
    security:
        - auth
    tags:
        - user
    parameters:
        - in: path
          name: logname
          description: Name of the log. Either a device ID, or one of predefined
          schema:
            type: string
            enum:
                - cr-drone-1
                - cr-hangar-1
                - security
                - schedules
                - accounts
          required: true
        - in: path
          name: from_ts
          description: UNIX timestamp, in seconds, of the time start of the entries
          schema:
            type: integer
            example: 1593500516
          required: true
        - in: path
          name: to_ts
          description: UNIX timestamp, in seconds, of the time end of the entries
          schema:
            type: integer
            example: 1593500516
          required: true
    definitions:
        AuditEntry:
            type: object
            properties:
                direction:
                    type: integer
                    description: >
                        Direction of the flow.

                            * 0 means the server sent this datagram

                            * 1 means the server received this datagram
                    enum: [0, 1]
                    nullable: true
                logname:
                    type: string
                    description: Either the device ID, or the name of the journal
                    enum:
                        - cr-drone-1
                        - cr-hangar-1
                        - security
                        - schedules
                        - accounts
                timestamp:
                    type: number
                    description: UNIX timestamp, in seconds
                    example: 1593507516
                payload:
                    type: object
                    description: >
                        Extra data. Please note that fields `what_happened` and `user` are
                        absent when dealing with per-device audits.
                    properties:
                        what_happened:
                            type: string
                            description: Machine readable message what happened
                        login:
                            type: string
                            nullable: true
                            format: email
                            description: Login of the user that caused this action to happen
                        ip:
                            type: string
                            description: IP from which the user has accessed this site
                            format: ipv4
                            example: 172.19.42.23
                        method:
                            type: string
                            description: HTTP method by which user tried to access this resource
                            example: post
                        url:
                            type: string
                            description: URL that the user tried to access
                            example: /v1/schedules
                    additionalProperties: true
            required:
                - logname
                - timestamp
                - payload
                - direction
            example:
                logname: schedules
                timestamp: 1593507516
                direction: null
                payload:
                    user: user@example.com
                    what_happened: manually_triggered
                    flight_id: 5beac29615364b57a92c0ffbc214212e
    responses:
        '200':
            description: Here goes your events list
            content:
                application/json:
                    schema:
                        type: array
                        items:
                            $ref: '#/definitions/AuditEntry'
        '403':
            description: Unauthorized
    """
    audit(AUDIT_SECURITY, {
        'login': session.user_id,
        'what_happened': 'audits_accessed',
        'logname': logname,
        'from': from_ts,
        'to': to_ts
    })
    return get(logname, from_ts, to_ts)


@user_blueprint.route('/v1/user/failures', methods=['GET'])
@as_json
@must_be_logged
def get_failures(session: Session) -> tp.List[Failure]:
    """
    Enumerate all failures experienced during this session
    ---
    security:
        - auth
    tags:
        - user
    definitions:
        Failure:
            type: object
            properties:
                user_id:
                    type: string
                    description: User to which given message was sent
                    example: user@example.com
                failure:
                    type: string
                    description: Machine readable message of failure
                    example: battery.not.charged
                datetime:
                    type: integer
                    format: int64
                    description: UNIX timestamp, in seconds, of when this occurred
                    example: 1593507516
                reason:
                    type: object
                    additionalProperties: true
                    description: An object with extra information
            required:
                - user_id
                - failure
                - datetime
                - reason
    responses:
        '200':
            description: Here goes your failure list
            content:
                application/json:
                    schema:
                        type: array
                        items:
                            $ref: '#/definitions/Failure'
        '403':
            description: Unauthorized
    """
    return Failure.get_all_failures(session.user_id)


@user_blueprint.route('/v1/user/failures', methods=['DELETE'])
@as_json
@must_be_logged
def delete_failures(session: Session):
    """
    Delete all failures
    ---
    security:
        - auth
    tags:
        - user
    responses:
        '200':
            description: Failures deleted
        '403':
            description: Unauthorized
    """
    for failure in Failure.get_all_failures(session.user_id):
        failure.delete()
