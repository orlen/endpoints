import time
import typing as tp

from flask import request, abort, Response, Blueprint

from audit import audit, AUDIT_ACCOUNTS, AUDIT_SECURITY
from dao import User, Session
from endpoints.decorators import must_be_logged, as_json

DEFAULT_EXPIRY_TIME = 60 * 60  # a hour

login_and_auth_blueprint = Blueprint('Login and auth', __name__)


@login_and_auth_blueprint.route("/v1/user/login", methods=['POST'])
def login() -> Response:
    """
    Logs in the user
    ---
    tags:
        - user
    requestBody:
          description: Data to login the user with
          required: true
          content:
            application/json:
                schema:
                    type: object
                    properties:
                      login:
                          type: string
                          format: email
                          description: User's email
                          example: "kowalski@example.com"
                      password:
                          type: string
                          description: User's password
                          example: "ala ma K0t@"
                          format: password
                    required:
                        - login
                        - password
    responses:
        '200':
            description: Login OK, here's your session ID
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            session_id:
                                type: string
                                description: The ID of user's session
                        required:
                            - session_id
            headers:
                Set-Cookie:
                    schema:
                        type: string
                        description: Your session cookie
                        example: session_id=213nfoie32r2d3d32d32; Path=/
        '401':
            description: Invalid login or password
    """
    data = request.get_json(force=True)

    try:
        user = User(data['login'])
    except (User.DoesNotExist, KeyError):
        audit(AUDIT_SECURITY, {
            'what_happened': 'wrong_login',
            'reason': 'invalid_user',
            'login': data.get('login', None),
            'ip': request.headers.get('X-Forwarded-For', '')
        })
        abort(403)

    if not user.check_password(data['password']):
        audit(AUDIT_SECURITY, {
            'what_happened': 'wrong_login',
            'reason': 'invalid_user',
            'login': data.get('login', None),
            'ip': request.headers.get('X-Forwarded-For', '')
        })
        abort(403)

    session_id = Session.create(user.user_id, time.time() + DEFAULT_EXPIRY_TIME)
    audit(AUDIT_SECURITY, {
        'what_happened': 'login',
        'login': user.user_id,
        'ip': request.headers.get('X-Forwarded-For', '')
    })
    resp = Response(status=200)
    resp.set_cookie('session_id', session_id, httponly=False)
    return resp


@login_and_auth_blueprint.route("/v1/user/new_password", methods=['POST'])
@as_json
@must_be_logged
def new_password(session: Session) -> None:
    """
    Changes the password for the current logged in user
    ---
    security:
        - auth
    tags:
        - user
    requestBody:
          required: true
          content:
            application/json:
                schema:
                    type: object
                    properties:
                      new_password:
                          type: string
                          description: New password
                          format: password
                          example: "alaM4H1V"
                    required:
                        - new_password
    responses:
        '200':
            description: Password changed
        '403':
            description: Unauthorized
    """
    json = request.get_json(force=True)
    session.user.set_password(json['new_password'])
    audit(AUDIT_ACCOUNTS, {
        'login': session.user_id,
        'what_happened': 'password changed'
    })


@login_and_auth_blueprint.route("/v1/user/logout", methods=['POST'])
@must_be_logged
def logout(session: Session) -> Response:
    """
    Logs out the user
    ---
    security:
        - auth
    tags:
        - user
    responses:
        '200':
            description: Logged out OK
        '403':
            description: Unauthorized
    """
    session.delete()
    a = Response(status=200)
    a.delete_cookie('session_id')
    audit(AUDIT_SECURITY, {
        'what_happened': 'logout',
        'ip': request.headers.get('X-Forwarded-For', '')
    })
    return a
