import logging
import typing as tp

from flask import request, Blueprint, Response
from dao import DailyScheduled, Session
from endpoints.decorators import as_json, must_be_logged
from audit import audit, AUDIT_SCHEDULES

logger = logging.getLogger(__name__)


dailyschedules_blueprint = Blueprint('Daily schedules', __name__)


@dailyschedules_blueprint.route('/v1/schedules', methods=['PUT'])
@must_be_logged
def create_new_schedule(session: Session) -> Response:
    """
    Add a new schedule position
    ---
    security:
        - auth
    tags:
        - user
    requestBody:
        content:
            application/json:
                schema:
                    $ref: '#/components/schemas/DailySchedule'
    responses:
        '201':
            description: Created OK
        '403':
            description: Unauthorized
        '409':
            description: Given schedule already exists
    """
    data = request.get_json(force=True, silent=True)

    seconds = data['seconds']
    drone_id = data['drone_id']
    hangar_id = data['hangar_id']
    user_id = data['user_id']

    try:
        DailyScheduled(seconds)
    except DailyScheduled.DoesNotExist:
        pass
    else:
        data.update(what_happened='added',
                    user=session.user_id)
        audit(AUDIT_SCHEDULES, data)
        return Response(status=409)  # Conflict! Resource already exists

    DailyScheduled.create(seconds, drone_id, hangar_id, user_id)

    return Response(status=201)


@dailyschedules_blueprint.route('/v1/schedules/<string:hangar_id>/<int:seconds>', methods=['DELETE'])
@as_json
@must_be_logged
def delete_schedule(session: Session, hangar_id: str, seconds: int) -> None:
    """
    Delete target schedule
    ---
    security:
        - auth
    tags:
        - user
    parameters:
        - in: path
          required: true
          schema:
            type: string
          name: hangar_id
          description: ID of the hangar
        - in: path
          required: true
          schema:
            type: integer
          name: seconds
          description: Seconds from midnight of given schedule
    responses:
        '200':
            description: Deleted OK
        '403':
            description: Unauthorized
        '404':
            description: Given schedule does not exist
    """
    try:
        ds = DailyScheduled(seconds)
    except DailyScheduled.DoesNotExist:
        return Response(status=404)

    ds.delete()
    audit(AUDIT_SCHEDULES, {
        'what_happened': 'removed',
        'login': session.user_id,
        'hangar_id': hangar_id,
        'seconds': seconds
    })


@dailyschedules_blueprint.route('/v1/schedules', methods=['GET'])
@as_json
@must_be_logged
def get_all_schedules(session: Session) -> tp.List[DailyScheduled]:
    """
    List all schedules
    ---
    security:
        - auth
    tags:
        - user
    responses:
        '200':
            description: Here's your list
            content:
                application/json:
                    schema:
                        type: array
                        items:
                            $ref: '#/components/schemas/DailySchedule'
        '403':
            description: Unauthorized
    """
    return DailyScheduled.get_all_schedules()
