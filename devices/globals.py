from collections import defaultdict

from common import SocketWrapper
from dao.exceptions import SocketClosed
from geventwebsocket.exceptions import WebSocketError


class DictSocketsFor(defaultdict):
    def __init__(self):
        super(DictSocketsFor, self).__init__(set)

    def append(self, key: str, sock: SocketWrapper):
        self._assert(key)
        self[key].add(sock)

    def is_any_open(self, key: str) -> bool:
        """
        Is the message able to be sent on at least one socket?

        This does not mean that the message will send successfully
        """
        self._assert(key)
        return any([not x.closed for x in self[key]])

    def _assert(self, key: str):
        try:
            self[key]
        except KeyError:
            self[key] = []

    def delete(self, key: str, sock: SocketWrapper):
        """Delete a given socket from a keying"""
        try:
            self[key].remove(sock)
        except (KeyError, ValueError):
            pass

    def send_to(self, key: str, message) -> bool:
        """Returns whether anything was sent"""
        self._assert(key)
        sent = False
        for sock in list(self[key]):
            try:
                sock.send(message)
                sent = True
            except (KeyError, SocketClosed, WebSocketError):
                try:
                    self[key].remove(sock)
                except KeyError:
                    pass
        return sent

    def send_all(self, message) -> bool:
        """Returns whether anything was sent"""
        keys = list(self.keys())
        sent = False
        for key in keys:
            sent = sent or self.send_to(key, message)
        return sent


SOCKETS_FOR_USERS = DictSocketsFor()
SOCKETS_FOR_DRONES = DictSocketsFor()
SOCKETS_FOR_HANGARS = DictSocketsFor()
