import json
import logging
import typing as tp
import math

from flask import request, Blueprint, Response

from audit import audit, AUDIT_SECURITY
from dao import Device, Session, Flight
from endpoints.decorators import as_json, must_be_logged
from endpoints.kvs import KVS
from endpoints.redis import redis
from .flight import FlightAggregate

logger = logging.getLogger(__name__)
authorization_blueprint: Blueprint = Blueprint('Authorization', __name__)


@authorization_blueprint.route('/v1/user/path', methods=['POST'])
@as_json
@must_be_logged
def get_flight_path(session: Session):
    """
    Return current flight path
    ---
    security:
        - auth
    tags:
        - user
    responses:
        '200':
            description: Path currently set
            content:
                application/json:
                    schema:
                        $ref: '#/components/schemas/Path'
        '403':
            description: Unauthorized
    """
    return json.loads(KVS['drone.path'])


@authorization_blueprint.route("/v1/drone/authorize_app", methods=['POST'])
@authorization_blueprint.route("/v1/hangar/authorize_app", methods=['POST'])
@as_json
def authorize_app() -> dict:
    """
    Provide a decryption key, if the checksum matches
    ---
    tags:
        - device
    requestBody:
        content:
            application/json:
                schema:
                    type: object
                    properties:
                        device_id:
                            type: string
                            description: ID of the device
                            example: cr-drone-id
                        checksum:
                            type: string
                            format: byte
                            description: Checksum
                    required:
                        - device_id
                        - checksum
    responses:
        '200':
            description: A response is available
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            status:
                                type: boolean
                                description: Whether the auth succeeded
                            key:
                                type: string
                                format: byte
                                description: Decryption key
                                nullable: true
                        required:
                            - status
                            - key
    """
    data = request.get_json(force=True, silent=False) or {}

    device_id = data['device_id']
    crc32 = data['checksum']

    try:
        device = Device(device_id)
    except Device.DoesNotExist:
        return {'status': False, 'key': None}

    device.ip = request.headers.get('X-Forwarded-For', '')

    if device.crc32 != crc32:
        return {'status': False, 'key': None}
    else:
        return {'status': True, 'key': device.auth_key}


@authorization_blueprint.route('/v1/drone/initialize', methods=['POST'])
@as_json
def initialize_drone() -> tp.Union[Response, dict]:
    """
    Give a path to the device
    ---
    tags:
        - device
    requestBody:
        content:
            application/json:
                schema:
                    type: object
                    properties:
                        device_id:
                            type: string
                            description: ID of the device
                            example: cr-drone-id
                        chunk:
                            type: integer
                            description: >
                                Chunk of the path. Is counted from 0, repeat the
                                query until empty "path" is returned.

                                If chunk is not given, entire path is returned. This
                                is done to circumvent the limit made by Orlen VPN on
                                length of replies.
                    required:
                        - device_id
    responses:
        '200':
            description: Path is available
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            flight_id:
                                type: string
                                description: ID of the flight
                            route:
                                type: string
                                format: json
                                description: Entire path (or just a part of it)
                        required:
                            - flight_id
                            - route
        '409':
            description: Flight not yet available
    """

    data = request.get_json(force=True, silent=False) or {}
    try:
        flight_id = redis.plain_get('DRONE.%s.FLIGHT' % (data['device_id'],))
        Device(data['device_id']).ip = request.headers.get('X-Forwarded-For', '')
    except KeyError:
        return Response(status=409)
    else:
        if 'chunk' in data:
            chunk = data['chunk']
            data = KVS['drone.path'][chunk * 500:(chunk + 1) * 500]
        else:
            data = KVS['drone.path']
        return {
            'flight_id': flight_id,
            'route': data
        }


@authorization_blueprint.route('/v1/wipe_all', methods=['POST'])
@as_json
@must_be_logged
def wipe_all(session: Session) -> dict:
    """
    Wipes all flights from the database and deletes all videos
    ---
    security:
        - auth
    tags:
        - manual
    responses:
        '200':
            description: Logged out OK
        '403':
            description: Unauthorized
    """
    for flight in Flight.get_all():
        flight.delete()
    audit(AUDIT_SECURITY, {
        'what_happened': 'flights_wiped',
        'login': session.user_id,
        'ip': request.headers.get('X-Forwarded-For', '')
    })
    return {'status': 'completed-successfully'}


@authorization_blueprint.route('/v1/set_route', methods=['POST'])
@as_json
@must_be_logged
def set_route(session: Session):
    """
    Set a new path for the drone
    ---
    security:
        - auth
    tags:
        - manual
    requestBody:
        required: true
        description: >
            New path to set. Note that hangar_yaw will be automatically scaled
            to be within range.
        content:
            application/json:
                schema:
                    $ref: '#/components/schemas/Path'
    responses:
        '200':
            description: Path changed
            content:
                application/json:
                    schema:
                        $ref: '#/components/schemas/Path'
        '403':
            description: Unauthorized
        '400':
            description: Invalid path
            content:
                application/json:
                    schema:
                        type: object
                        properties:
                            status:
                                type: string
                                description: Description of the error
                                example: key zones not found
                        required:
                            - status
    """
    new_path = request.get_json(force=True)
    prev_path = json.loads(KVS['drone.path'])
    while new_path['hangar_yaw'] < -math.pi:
        new_path['hangar_yaw'] += 2*math.pi
    while new_path['hangar_yaw'] > math.pi:
        new_path['hangar_yaw'] -= 2*math.pi
    for key in ['user_points', 'zones', 'route_points', 'start_point', 'end_point']:
        if key not in new_path:
            return {'status': 'key %s not found' % (key, )}, 400
    KVS['drone.path'] = json.dumps(new_path)
    audit(AUDIT_SECURITY, {
        'what_happened': 'path_changed',
        'new_path': new_path,
        'previous_path': prev_path,
        'login': session.user_id,
        'ip': request.headers.get('X-Forwarded-For', '')
    })
    return new_path


@authorization_blueprint.route("/v1/abort", methods=['POST'])
@as_json
@must_be_logged
def abort_all_flights(session: Session) -> dict:
    """
    Abort all flights in progress
    ---
    security:
        - auth
    tags:
        - manual
    responses:
        '200':
            description: Path changed
        '403':
            description: Unauthorized
    """
    for flight_id in redis.hgetall('FLIGHTS'):
        flight_id = flight_id.decode('utf8')
        FlightAggregate(flight_id).terminate_flight()
        audit(AUDIT_SECURITY, {
            'what_happened': 'flight_aborted',
            'flight_id': flight_id,
            'login': session.user_id,
            'ip': request.headers.get('X-Forwarded-For', '')
        })
    redis.flushall()
    return {'status': 'completed-successfully'}
