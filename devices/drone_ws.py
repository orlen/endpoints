import logging

from flask import Blueprint, request

from common import SocketWrapper
from dao import Flight, SocketClosed
from endpoints.decorators import silence_and_log_exceptions
from .flight import FlightAggregate
from .flight.command_manager import CommandManager
from .globals import SOCKETS_FOR_USERS, SOCKETS_FOR_DRONES

drone_ws_blueprint = Blueprint('Drone WS', __name__)
logger = logging.getLogger(__name__)


@drone_ws_blueprint.route('/v1/drone/websocket/<string:flight_id>')
@silence_and_log_exceptions
def websocket_drone(ws, flight_id) -> None:
    try:
        flight = Flight(flight_id)
    except Flight.DoesNotExist as e:
        ws.close()
        return

    flight.drone.ip = request.headers.get('X-Forwarded-For', '')

    logger.info('Got a drone connection on %s', flight_id)
    ws = SocketWrapper(ws, flight.drone_id)
    SOCKETS_FOR_DRONES.append(flight.drone_id, ws)

    while not ws.closed:
        try:
            message = ws.receive()
            logger.info('Received packet from drone %s', repr(message))
        except SocketClosed:
            break

        # Parse the message
        if message['message_type'] == 'stream':
            SOCKETS_FOR_USERS.send_to(flight.user_id, message)
        elif message['message_type'] == 'state_change':
            FlightAggregate(flight.flight_id).state.on_drone_state_change(message['state_change']['new_state'])
            SOCKETS_FOR_USERS.send_all(message)
        elif message['message_type'] == 'command_response':
            CommandManager().on_response(flight.drone_id, 'drone', message['command_response']['command_name'])

    flight.drone.state = 'off'
    SOCKETS_FOR_DRONES.delete(flight.drone_id, ws)
    logger.info('Lost connection from the drone, forcing state to off')
