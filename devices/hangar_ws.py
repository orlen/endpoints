import json
import logging

from flask import Blueprint, request

from common import SocketWrapper
from dao import Device
from dao.exceptions import SocketClosed
from endpoints.redis import redis
from endpoints.decorators import silence_and_log_exceptions
from .flight import FlightAggregate
from .globals import SOCKETS_FOR_HANGARS, SOCKETS_FOR_USERS

hangar_ws_blueprint = Blueprint('Hangar WS', __name__)
logger = logging.getLogger(__name__)


@hangar_ws_blueprint.route('/v1/hangar/websocket')
@silence_and_log_exceptions
def websocket_hangar(ws):
    message = json.loads(ws.receive())
    try:
        hangar = Device(message['device_id'])
    except Device.DoesNotExist as e:
        ws.close()
        return

    hangar.ip = request.headers.get('X-Forwarded-For', '')

    ws = SocketWrapper(ws, message['device_id'])
    SOCKETS_FOR_HANGARS.append(message['device_id'], ws)

    while not ws.closed:
        # Parse the message
        if message['message_type'] == 'heartbeat':
            drone_battery = message['heartbeat']['drone_battery']
            if drone_battery is None:
                output = '0'
            else:
                if not drone_battery['charging'] and message['heartbeat']['state'] == 'hangar_full':
                    output = '1'
                    logger.info('Hangar reported battery being ready')
                else:
                    output = '0'

            redis.plain_set('HANGAR.%s.BATTERY' % (hangar.device_id,), output)
            try:
                flight_id = redis.plain_get('HANGAR.%s.FLIGHT' % (hangar.device_id,))
            except KeyError:
                SOCKETS_FOR_USERS.send_all(message)
            else:
                fa = FlightAggregate(flight_id)
                SOCKETS_FOR_USERS.send_to(fa.flight.user_id, message)
                fa.state.on_heartbeat()

        elif message['message_type'] == 'state_change':
            try:
                flight_id = redis.plain_get('HANGAR.%s.FLIGHT' % (hangar.device_id,))
            except KeyError:
                SOCKETS_FOR_USERS.send_all(message)
            else:
                FlightAggregate(flight_id).state.on_hangar_state_change(message['state_change']['new_state'])
                SOCKETS_FOR_USERS.send_all(message)
        elif message['message_type'] == 'command_response':
            from .flight.command_manager import CommandManager
            CommandManager().on_response(hangar.device_id, 'hangar', message['command_response']['command_name'])

        try:
            message = ws.receive()
            logger.info('Got message from hangar %s', repr(message))
        except SocketClosed:
            logger.info('Lost connection from the hangar')
            ws.close()
            break

    SOCKETS_FOR_HANGARS.delete(hangar.device_id, ws)
