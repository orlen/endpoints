import logging
import time
import typing as tp
import collections
from satella.json import JSONAble
from flask import Blueprint
from satella.coding.structures import Singleton

from endpoints.decorators import cron
from ..globals import SOCKETS_FOR_DRONES, SOCKETS_FOR_HANGARS, DictSocketsFor

logger = logging.getLogger(__name__)

command_manager_blueprint = Blueprint('CommandManagerBlueprint', __name__)


class ServerCommand(JSONAble):
    def __init__(self, name: str, operator_id: str = 'system', parameters: dict = None, timestamp: int = None):
        self.parameters: dict = parameters or {}
        self.name: str = name
        self.operator_id: str = operator_id
        self.timestamp: int = timestamp or int(time.time() * 1000)

    def to_json(self) -> dict:
        return {
            'parameters': self.parameters,
            'name': self.name,
            'operator_id': self.operator_id,
            'timestamp': self.timestamp
        }


class Command:
    RESEND_EACH = 20.0

    def __init__(self, target: str, name: str, device_id: str, *args):
        assert target in ('hangar', 'drone')
        self.target: str = target
        self.name: str = name
        self.last_sent: float = time.time()
        self.device_id: str = device_id
        self.args: tuple = args

    def send(self) -> None:
        sockets: DictSocketsFor = SOCKETS_FOR_DRONES if self.target == 'drone' else SOCKETS_FOR_HANGARS
        sc = ServerCommand(self.name, *self.args)
        sockets.send_to(self.device_id, sc)
        self.last_sent = time.time()

    def try_resend(self) -> None:
        if time.time() - self.last_sent > Command.RESEND_EACH:
            self.send()


@Singleton
class CommandManager:
    def __init__(self):
        # a dict of device_id => dict of command_name => Command
        self.outstanding_commands_to_drone: tp.Dict[str, tp.Dict[str, Command]] = collections.defaultdict(dict)
        self.outstanding_commands_to_hangar: tp.Dict[str, tp.Dict[str, Command]] = collections.defaultdict(dict)

    def get_dict_based_on_type(self, type_: str) -> dict:
        if type_ == 'hangar':
            return self.outstanding_commands_to_hangar
        elif type_ == 'drone':
            return self.outstanding_commands_to_drone
        else:
            raise ValueError('Unknown type_ %s' % (type_, ))

    def on_heartbeat(self) -> None:
        for drone_id in self.outstanding_commands_to_drone.keys():
            for outstanding_command_to_drone in self.outstanding_commands_to_drone[drone_id].values():
                outstanding_command_to_drone.try_resend()
        for hangar_id in self.outstanding_commands_to_hangar.keys():
            for outstanding_command_to_hangar in self.outstanding_commands_to_hangar[hangar_id].values():
                outstanding_command_to_hangar.try_resend()

    def clear_for(self, type_, device_id) -> None:
        if type_ == 'hangar':
            self.outstanding_commands_to_hangar[device_id] = {}
        elif type_ == 'drone':
            self.outstanding_commands_to_drone[device_id] = {}

    def send_to(self, target, device_id, name, *args) -> None:
        outs = self.get_dict_based_on_type(target)
        outs[device_id][name] = Command(target, name, device_id, *args)
        try:
            outs[device_id][name].send()
        except KeyError:
            logger.error("Failed sending command %s", name)

    def on_response(self, device_id: str, target: str, name: str) -> None:
        outs = self.get_dict_based_on_type(target)[device_id]

        if name in outs:
            del outs[name]
        else:
            logger.warning('Got a reply for a command never issued from %s (%s) regarding %s', device_id, target, name)


@cron(10)
def resend_commands() -> None:
    CommandManager().on_heartbeat()
