import logging
import time

from dao import Flight, SocketClosed
from endpoints.redis import redis
from ..globals import SOCKETS_FOR_USERS
from .command_manager import CommandManager
import geventwebsocket.exceptions
from videograbber.videograbber_control import VideoGrabberControls

logger = logging.getLogger(__name__)

SECONDS_REMAINING_IN_WAIT: int = 4 * 60


class FlightAggregate:
    def __init__(self, flight_id):
        self.flight = Flight(flight_id)
        self.state = FlightStateMachinery(self)
        self.command_manager = CommandManager()

    @staticmethod
    def is_flight_running() -> bool:
        return len(redis.hgetall('FLIGHTS')) > 0

    def is_waiting_for_user_confirmation(self) -> bool:
        return self.flight.status == 1

    def initialize(self) -> None:
        """
        Mark as pickupable by the drone
        """
        self.command_manager.clear_for('drone', self.flight.drone_id)
        self.command_manager.clear_for('hangar', self.flight.hangar_id)
        redis.hset('FLIGHTS', self.flight.flight_id, '1')
        redis.plain_set('USER.%s.FLIGHT' % (self.flight.user_id,), self.flight.flight_id)
        redis.plain_set('DRONE.%s.FLIGHT' % (self.flight.drone_id,), self.flight.flight_id)
        redis.plain_set('HANGAR.%s.FLIGHT' % (self.flight.hangar_id,), self.flight.flight_id)
        redis.plain_set('FLIGHT.%s.SECONDS_REMAINING_IN_WAIT' % (self.flight.flight_id,),
                        str(SECONDS_REMAINING_IN_WAIT))
        redis.plain_set('FLIGHT.%s.LAST_CHECKED' % (self.flight.flight_id,), str(int(time.time())))
        self.flight.status = Flight.STATUS_INITIALIZED
        self.command_manager.send_to('hangar', self.flight.hangar_id, 'init_flight', 'system', {
            'flight_id': self.flight.flight_id
        })

    def terminate_flight(self, did_the_flight_happen: bool = True) -> None:
        if did_the_flight_happen:
            VideoGrabberControls().stop_flight(self.flight.flight_id)
        self.flight.status = Flight.STATUS_TERMINATED if did_the_flight_happen else Flight.STATUS_CANCELLED
        redis.hdel('FLIGHTS', self.flight.flight_id)
        redis.delete('USER.%s.FLIGHT' % (self.flight.user_id,))
        redis.delete('DRONE.%s.FLIGHT' % (self.flight.drone_id,))
        redis.delete('HANGAR.%s.FLIGHT' % (self.flight.hangar_id,))
        self.flight.drone.state = 'off'
        self.flight.hangar.state = 'off'
        redis.delete('FLIGHT.%s.SECONDS_REMAINING_IN_WAIT' % (self.flight.flight_id,))
        redis.delete('FLIGHT.%s.LAST_CHECKED' % (self.flight.flight_id, ))
        try:
            SOCKETS_FOR_USERS.send_to(self.flight.user_id, {'type': 'flight-finished'})
        except (SocketClosed, KeyError):
            pass


class FlightStateMachinery:
    def __init__(self, flight_aggregate: FlightAggregate):
        self.flight_aggregate: FlightAggregate = flight_aggregate
        self.command_manager = CommandManager()

        def on_confirm() -> None:
            self.flight_aggregate.flight.status = Flight.STATUS_PENDING_CONFIRMATION
            try:
                SOCKETS_FOR_USERS.send_to(self.flight_aggregate.flight.user_id, {"type": "flight-confirm"})
            except (KeyError, SocketClosed, geventwebsocket.exceptions.WebSocketError):
                pass

        def on_leave_drone() -> None:
            logger.warning("Leave drone sent to the hangar")
            self.command_manager.send_to('hangar', self.flight_aggregate.flight.hangar_id, 'leave_drone')

        def on_approach_cruise() -> None:
            self.command_manager.send_to('hangar', self.flight_aggregate.flight.hangar_id, 'receive_drone', 'system', {
                'flight_id': self.flight_aggregate.flight.flight_id,
                'drone_id': self.flight_aggregate.flight.drone_id
            })

        def on_drone_started() -> None:
            self.command_manager.send_to('hangar', self.flight_aggregate.flight.hangar_id, 'drone_started')

        def land() -> None:
            self.command_manager.send_to('drone', self.flight_aggregate.flight.drone_id, 'land')

        def landed() -> None:
            VideoGrabberControls().stop_flight(self.flight_aggregate.flight.flight_id)
            self.command_manager.send_to('hangar', self.flight_aggregate.flight.hangar_id, 'drone_landed')

        def shutdown_drone() -> None:
            self.command_manager.send_to('hangar', self.flight_aggregate.flight.hangar_id, 'shutdown_drone')

        def finish_flight() -> None:
            self.flight_aggregate.terminate_flight()

        self.STATE_MACHINE = {
            ('ready', 'drone_prepared'): on_confirm,  # request the user to confirm the flight
            ('cruise', 'protect_drone'): on_leave_drone,  # sends leave_drone to the hangar
            ('approach_cruise', 'hangar_empty'): on_approach_cruise,  # sends receive_drone to the hangar
            ('takeoff', 'drone_prepared'): on_drone_started,  # send drone_started to hangar
            ('approach_waiting', 'waiting_drone'): land,  # sends land to the drone
            ('rth_approach_waiting', 'waiting_drone'): land,  # sends land to the drone
            ('rth_approach_cruise', 'hangar_empty'): on_approach_cruise,  # sends receive_drone to the hangar
            ('landed', 'waiting_drone'): landed,  # sends drone_landed to the hangar
            ('rth_landed', 'waiting_drone'): landed,  # sends drone_landed to the hangar
            ('shutdown', 'holding_drone'): shutdown_drone,  # sends shutdown_drone to the hangar
            ('off', 'holding_drone'): shutdown_drone,  # remove the flight from current list of flights
            ('shutdown', 'battery_charging'): finish_flight,  # remove the flight from current list of flights
            ('off', 'battery_charging'): finish_flight,  # remove the flight from current list of flights
        }

    def get_drone_state(self) -> str:
        try:
            return self.flight_aggregate.flight.drone.state
        except KeyError:
            return 'off'

    def get_hangar_state(self) -> str:
        try:
            return self.flight_aggregate.flight.hangar.state
        except KeyError:
            return 'hangar_full'

    @property
    def seconds_remaining(self) -> float:
        x = redis.plain_get('FLIGHT.%s.SECONDS_REMAINING_IN_WAIT' % (self.flight_aggregate.flight.flight_id,),
                            str(SECONDS_REMAINING_IN_WAIT))
        return float(x)

    @seconds_remaining.setter
    def seconds_remaining(self, value: float):
        redis.plain_set('FLIGHT.%s.SECONDS_REMAINING_IN_WAIT' % (self.flight_aggregate.flight.flight_id,),
                        str(value))

    @property
    def last_checked(self) -> int:
        x = redis.plain_get('FLIGHT.%s.LAST_CHECKED' % (self.flight_aggregate.flight.flight_id,),
                            str(int(time.time())))
        return int(x)

    @last_checked.setter
    def last_checked(self, value: float):
        redis.plain_set('FLIGHT.%s.LAST_CHECKED' % (self.flight_aggregate.flight.flight_id,), str(int(value)))

    def on_drone_state_change(self, to_state: str):
        self.flight_aggregate.flight.drone.state = to_state

        if to_state == 'manual_control':
            self.command_manager.send_to('hangar', self.flight_aggregate.flight.hangar_id, 'manual_close')
            self.flight_aggregate.terminate_flight()
            logger.warning('Terminating flight due to manual_control seen')
        else:
            hangar_state = self.get_hangar_state()
            logger.warning('Processing state machine of %s and %s', to_state, hangar_state)
            try:
                self.STATE_MACHINE[(to_state, hangar_state)]()
            except KeyError:
                pass

    def on_hangar_state_change(self, to_state: str):
        self.flight_aggregate.flight.hangar.state = to_state

        drone_state = self.get_drone_state()
        logger.warning('Processing state machine of %s and %s', drone_state, to_state)
        try:
            self.STATE_MACHINE[(drone_state, to_state)]()
        except KeyError:
            pass

    def on_heartbeat(self):
        if self.get_drone_state() in ('takeoff-waiting', 'rth_takeoff-waiting', 'cruise-waiting', 'rth_cruise-waiting',
                                      'rth_approach_cruise-waiting', 'approach_cruise-waiting'):
            time_left = time.time() - self.last_checked
            sec_rem = self.seconds_remaining
            if time_left > sec_rem:
                # Return to home, forbid from
                self.command_manager.send_to('drone', self.flight_aggregate.flight.drone_id, 'continue_flight')
                self.seconds_remaining = 0
                try:
                    SOCKETS_FOR_USERS.send_to(self.flight_aggregate.flight.user_id, {'type': 'forced-flight'})
                except (SocketClosed, KeyError):
                    pass
            else:
                self.seconds_remaining = sec_rem - time_left
        self.last_checked = time.time()

    def on_user_command(self, command: str) -> bool:
        assert command in ('stop', 'resume', 'abort', 'authorize', 'cancel')

        if command == 'cancel':
            if self.flight_aggregate.flight.status != Flight.STATUS_PENDING_CONFIRMATION:
                logger.warning('Command authorize issued in invalid state, ignoring')
                return False

            self.command_manager.send_to('hangar', self.flight_aggregate.flight.hangar_id, 'cancel_flight')
            self.command_manager.send_to('drone', self.flight_aggregate.flight.drone_id, 'cancel_flight')
            self.flight_aggregate.terminate_flight(did_the_flight_happen=False)
            self.flight_aggregate.flight.approve(self.flight_aggregate.flight.user_id, Flight.APROV_REJECTED)

        elif command == 'authorize':
            if self.flight_aggregate.flight.status != Flight.STATUS_PENDING_CONFIRMATION:
                logger.warning('Command authorize issued in invalid state, ignoring')
                return False

            self.flight_aggregate.flight.status = Flight.STATUS_CONFIRMED_OPERATOR
            self.flight_aggregate.flight.stream_port = VideoGrabberControls().start_for_flight(self.flight_aggregate.flight.flight_id)
            self.command_manager.send_to('drone', self.flight_aggregate.flight.drone_id, 'start_flight')

        elif command == 'stop':
            self.last_checked = time.time()
            if self.seconds_remaining > 0:
                self.command_manager.send_to('drone', self.flight_aggregate.flight.drone_id, 'wait')
            else:
                return False

        elif command == 'resume':
            time_sub = time.time() - self.last_checked
            if time_sub > self.seconds_remaining:
                self.seconds_remaining = 0
            else:
                self.seconds_remaining = self.seconds_remaining - time_sub
            self.command_manager.send_to('drone', self.flight_aggregate.flight.drone_id, 'continue_flight')

        elif command == 'abort':
            self.command_manager.send_to('drone', self.flight_aggregate.flight.drone_id, 'return_home')

        return True
