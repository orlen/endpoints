"""
Supporting endpoints for drone/hangar
"""

from devices.authorization import authorization_blueprint
from devices.drone_ws import drone_ws_blueprint
from devices.flight.command_manager import command_manager_blueprint
from devices.hangar_ws import hangar_ws_blueprint
from devices.log_dumps import log_dumps_blueprint

blueprints = [authorization_blueprint, drone_ws_blueprint, hangar_ws_blueprint, log_dumps_blueprint,
              command_manager_blueprint]
