from flask import request, abort, Blueprint

from dao import Device, DeviceDataFileAttached
from endpoints.decorators import as_json

log_dumps_blueprint = Blueprint('Log dump', __name__)


@log_dumps_blueprint.route('/v1/drone/dump_logs/<string:device_id>', methods=['POST'])
@log_dumps_blueprint.route('/v1/drone/alarm/<string:device_id>', methods=['POST'])
@log_dumps_blueprint.route('/v1/hangar/dump_logs/<string:device_id>', methods=['POST'])
@log_dumps_blueprint.route('/v1/hangar/alarm/<string:device_id>', methods=['POST'])
@as_json
def drone_logs_and_alarm(device_id: str) -> dict:
    """
    Submit device logs.

    Attach your files using multipart and name them "file"
    ---
    tags:
        - device
    parameters:
        - in: path
          name: device_id
          schema:
            type: string
            example: cr-drone-1
          description: Identifier of the device adding it
          required: true
    requestBody:
        content:
            multipart/form-data:
                schema:
                    type: object
                    properties:
                        file:
                            type: string
                            format: binary
    responses:
            '200':
                description: Log uploaded
                content:
                    application/json:
                        schema:
                            type: object
                            properties:
                                file_id:
                                    type: string
                                    format: uuid
                                    description: Identifier of added file
                                    example: d23324ff4r23d23d23
                            required:
                                - file_id
            '404':
                description: Invalid device
            '400':
                description: >
                    File not attached
    """

    try:
        device = Device(device_id)
    except Device.DoesNotExist:
        return abort(404)

    if 'file' not in request.files:
        return abort(400)

    datafile_id = DeviceDataFileAttached.create(device,
                                                DeviceDataFileAttached.ALARM if 'alarm' in request.url else DeviceDataFileAttached.LOG,
                                                request.files['file'].read())

    return {'file_id': datafile_id}
