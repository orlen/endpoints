import datetime
import logging
import time
from flask import Response

from dao import DailyScheduled, Flight, Failure
from devices.flight import FlightAggregate
from endpoints.decorators import cron
from endpoints.kvs import KVS
from endpoints.redis import redis
from .globals import SOCKETS_FOR_HANGARS

logger = logging.getLogger(__name__)

CDD_KVS_ID = 'cron.last_day_schedule_generated_for'


@cron(30.0)
def check_daily_schedules() -> Response:
    previous_day: int = int(KVS.get(CDD_KVS_ID, '0'))

    today_dt: datetime.datetime = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    today: int = int(time.mktime(today_dt.utctimetuple()))

    if previous_day != today:
        for schedule in DailyScheduled.get_all_schedules():
            logger.warning('Creating schedule %s', schedule)
            schedule.create_flight_for(today)
    else:
        return Response(status=200)

    KVS[CDD_KVS_ID] = today
    return Response(status=201)


@cron(10.0)
def pick_up_flights() -> None:
    # Is a flight currently in progress?
    if FlightAggregate.is_flight_running():
        return

    # Enumerate flights capable of pick-up
    for flight in Flight.choose(0, time.time(), 'DESC', 20):
        if flight.datetime < time.time() and flight.status == Flight.STATUS_WAITING:
            logger.info('It is due time for flight %s', flight.flight_id)

            # is another flight running?
            if len(redis.hgetall('FLIGHTS')) > 0:
                Failure.create(flight.user_id, 'flight.running')
                logger.warning('Flight cancelled due to another flight being in progress')
                flight.status = Flight.STATUS_CANCELLED
                return

            # is that socket registered?
            if flight.hangar_id not in SOCKETS_FOR_HANGARS:
                Failure.create(flight.user_id, 'hangar.disconnected', {'reason': 'no-websocket'})
                logger.warning('Flight rejected due to no hangar present')
                return

            # is that socket open?
            if not SOCKETS_FOR_HANGARS.is_any_open(flight.hangar_id):
                Failure.create(flight.user_id, 'hangar.disconnected', {'reason': 'websocket-closed'})
                logger.warning('Flight rejected due to no hangar websocket being closed')
                return

            # is battery loaded?
            p = redis.plain_get('HANGAR.%s.BATTERY' % (flight.hangar_id,), '0')
            if p == '0':
                Failure.create(flight.user_id, 'battery.not.charged')
                logger.warning('Flight rejected due to battery issues')
                return  # battery is not charged

            # pick that flight up
            logger.warning('Initializing flight %s', flight.flight_id)
            FlightAggregate(flight.flight_id).initialize()
