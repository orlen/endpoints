# Deployment patterns

This should be deployed as a single (1) instance, as it does not support
any kind of cross-instance locking.

# API Endpoints
[![pipeline status](https://git.cervirobotics.com/dronhub/orlen-backend/endpoints/badges/develop/pipeline.svg)](https://git.cervirobotics.com/dronhub/orlen-backend/endpoints/commits/develop)
[![coverage report](https://git.cervirobotics.com/dronhub/orlen-backend/endpoints/badges/develop/coverage.svg)](https://git.cervirobotics.com/dronhub/orlen-backend/endpoints/commits/develop)

Go [read the docs](docs/README.md).

Remember to explicitly set the environment variable
`DEBUG` to `False` on deployment onto production,
otherwise the weather won't work.

Review the [change log](CHANGELOG.md) for details like what has been when changed.