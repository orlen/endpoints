import collections
import logging
import typing as tp
import requests
import requests.exceptions

"""
A library to use the microservices of audit.
Download from: https://git.cervirobotics.com/dronhub/orlen-backend/audit/snippets/1

This will stay silent upon failures, and will simply log an entry to the log
"""

logger = logging.getLogger(__name__)

__all__ = ['send', 'recv', 'audit', 'get',
           'AUDIT_SECURITY', 'AUDIT_ACCOUNTS',
           'AUDIT_SCHEDULES',
           'LogEntry']

AUDIT_SECURITY = 'security'  # log-ins and failed log-ins
AUDIT_ACCOUNTS = 'accounts'  # changing passwords, adding accounts, admin log-in
AUDIT_SCHEDULES = 'schedules'

_O = collections.namedtuple('_O', ('status_code',))(204)


class LogEntry:
    def __init__(self, logname: str, timestamp: float, payload: dict, direction: tp.Optional[int]):
        self.logname = logname
        self.timestamp = timestamp
        self.payload = payload
        self.direction = direction

    def to_json(self) -> dict:
        return {
            'logname': self.logname,
            'timestamp': self.timestamp,
            'payload': self.payload,
            'direction': self.direction
        }

_ALLOWED_JOURNALS: tp.List[str] = [AUDIT_SECURITY, AUDIT_SCHEDULES, AUDIT_ACCOUNTS]


def get(logname: str, from_: float, to: float) -> tp.List[LogEntry]:
    """
    Obtain log entries from the journal

    :param logname: name of the log to use
    :param from_: timestamp, time of start
    :param to: timestamp, time of stop
    """
    c = requests.post('http://audit/v1/get/' + logname, json={
        'from': from_,
        'to': to
    })
    if c.status_code != 200:
        raise IOError('status code equals %s', (c.status_code,))
    return [LogEntry(**x) for x in c.json]


def send(logname: str, frame: dict) -> None:
    """
    Log that a specified frame has been sent to target device
    :param logname: device_id of the target
    :param frame: JSON frame sent
    """
    try:
        c = requests.post('http://audit/v1/send/' + logname, json=frame)
    except requests.exceptions.ConnectionError:
        c = _O
    if c.status_code != 200:
        logger.warning('Failed to audit sent packet %s', repr(frame))


def recv(frame: dict) -> None:
    """
    Log that a given frame has been received via websockets
    :param frame: frame received
    """
    try:
        c = requests.post('http://audit/v1/recv', json=frame)
    except requests.exceptions.ConnectionError:
        c = _O
    if c.status_code != 200:
        logger.warning('Failed to audit received packet %s' % (frame,))


def audit(logname: str, frame: dict) -> None:
    """
    Log that a given audit event has occurred in the system
    :param logname: name of audit log
    :param frame: JSON payload to log
    """
    assert logname in _ALLOWED_JOURNALS, 'Invalid logname'

    try:
        c = requests.post('http://audit/v1/log/' + logname, json=frame)
    except requests.exceptions.ConnectionError:
        c = _O

    if c.status_code != 200:
        logger.warning('Failed to audit a frame %s to journal %s, got RC of %s', frame, logname, c.status_code)
