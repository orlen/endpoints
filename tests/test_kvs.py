import unittest

from endpoints.kvs import KVS
from endpoints.redis import redis

from tests import BaseTestCase


class TestKVS(unittest.TestCase):
    def inner_kvs(self, kvs: dict):
        kvs['store'] = '5'
        self.assertEquals(kvs['store'], '5')
        self.assertEquals(kvs.get('store', '4'), '5')
        del kvs['store']
        del kvs['store']  # double deletes should go through without any trouble
        self.assertRaises(KeyError, lambda: kvs['store'])
        self.assertEquals(kvs.get('store', '4'), '4')

    def test_cassandra_kvs(self):
        self.inner_kvs(KVS)

    def test_plain_get_and_set(self):
        redis.delete('dupa')
        default = redis.plain_get('dupa', 'default')
        self.assertEquals(default, 'default')
        redis.plain_set('dupa', 'value')
        self.assertEquals(redis.plain_get('dupa'), 'value')


class TestVersion(BaseTestCase):
    def test_version(self):
        from endpoints import VERSION, BUILD
        x = self.post('/v1/version')
        self.assertEquals(x.json(), {'version': VERSION,
                                     'build': BUILD
                                     })

    def test_apispec(self):
        self.assertEqual(self.get('/apispec_1.json').status_code, 200)
