import unittest
import uuid

import requests

from dao import *
from tests import AppThread


class TestLogUpload(unittest.TestCase):
    def setUp(self):
        AppThread.check_if_running()
        self.session = requests.Session()
        self.CONTENT = uuid.uuid4().hex.encode('utf8')

    def test_hangar_log_upload(self):
        multipart_form_data = {
            'file': ('custom_file_name.zip', self.CONTENT, 'application/octet'),
        }

        response = requests.post('http://127.0.0.1/v1/hangar/dump_logs/cr-hangar-1', files=multipart_form_data)

        self.assertEquals(response.status_code, 200)
        datafile_id = response.json()['file_id']
        self.dfa = DeviceDataFileAttached(datafile_id)
        self.assertEquals(self.dfa.device_id, 'cr-hangar-1')
        self.assertEquals(self.dfa.content, self.CONTENT)

    def test_flight_log_upload(self):
        multipart_form_data = {
            'file': ('custom_file_name.zip', self.CONTENT, 'application/octet'),
        }

        response = requests.post('http://127.0.0.1/v1/drone/dump_logs/cr-drone-1',
                                 files=multipart_form_data)

        self.assertEquals(response.status_code, 200)
        datafile_id = response.json()['file_id']
        self.dfa = DeviceDataFileAttached(datafile_id)
        self.assertEquals(self.dfa.device_id, 'cr-drone-1')
        self.assertEquals(self.dfa.content, self.CONTENT)

    def test_flight_alarm_upload(self):
        multipart_form_data = {
            'file': ('custom_file_name.zip', self.CONTENT, 'application/octet'),
        }

        response = requests.post('http://127.0.0.1/v1/drone/alarm/cr-drone-1',
                                 files=multipart_form_data)

        self.assertEquals(response.status_code, 200)
        datafile_id = response.json()['file_id']
        self.dfa = DeviceDataFileAttached(datafile_id)
        self.assertEquals(self.dfa.device_id, 'cr-drone-1')
        self.assertEquals(self.dfa.content, self.CONTENT)
        self.assertEquals(self.dfa.file_type, DataFileAttached.ALARM)
