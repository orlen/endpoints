from tests import BaseTestCase


class TestDeviceAuth(BaseTestCase):
    def test_auth_drone(self):
        resp = self.post('/v1/drone/authorize_app', {
            'device_id': 'cr-drone-1',
            'checksum': 'YWYzNA==',
        })
        self.assertEquals(resp.status_code, 200)
        resp = resp.json()
        self.assertEquals(resp['status'], True)
        self.assertEquals(resp['key'], 'bXkgYXV0aCBrZXk=')

    def test_auth_drone_fail(self):
        resp = self.post('/v1/drone/authorize_app', {
            'device_id': 'cr-drone-1',
            'checksum': 'YWYzNAKs==',
        })
        self.assertEquals(resp.status_code, 200)
        resp = resp.json()
        self.assertEquals(resp['status'], False)
        self.assertEquals(resp['key'], None)
