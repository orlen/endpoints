import json
import logging
import threading
import unittest

import requests

from endpoints.app import run_application  # at this point monkey-patching for gevent is done

logging.basicConfig(level=logging.DEBUG)

__all__ = ['BaseTestCase', 'AppThread']


class AppThread(threading.Thread):
    def __init__(self):
        super(AppThread, self).__init__()
        self.daemon = True

    def run(self):
        run_application(logging_level=logging.DEBUG)

    APP_THREAD: 'AppThread' = None

    @classmethod
    def check_if_running(cls):
        if cls.APP_THREAD is None:
            cls.APP_THREAD = AppThread()
            cls.APP_THREAD.start()


class BaseTestCase(unittest.TestCase):
    LOGIN_ME_AS_WELL = False

    def setUp(self):
        super(BaseTestCase, self).setUp()
        AppThread.check_if_running()
        self.session = requests.Session()
        self.delete = lambda url, data=None: self.session.delete('http://127.0.0.1' + url)
        self.patch = lambda url, data=None: self.session.patch('http://127.0.0.1' + url, data=json.dumps(data or {}),
                                                             headers={'Content-Type': 'application/json'})
        self.put = lambda url, data=None: self.session.put('http://127.0.0.1' + url, data=json.dumps(data or {}),
                                                         headers={'Content-Type': 'application/json'})
        self.post = lambda url, data=None: self.session.post('http://127.0.0.1' + url, data=json.dumps(data or {}),
                                                           headers={'Content-Type': 'application/json'})
        self.get = lambda url, data=None: self.session.get('http://127.0.0.1' + url, params=data or {})
        if self.LOGIN_ME_AS_WELL:
            self.post('/v1/user/login', {'login': 'root@example.com',
                                         'password': '1234'})


from . import test_user
from . import test_devices
from . import test_dao
from . import test_kvs
