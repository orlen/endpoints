#!/bin/bash
set -e

if [[ ! -z "${GITLAB_CI_USED}" ]]; then
# Sleep if running under GitLab CI
# When developing locally, Cassandra will survive re-boots so we can disregard
# that to have faster build times.
# Of course first test locally will fail due to Cassandra not being ready
# but we can deal with that. Tools are supposed to be used by humans.
  echo "Waiting for Cassandra to start up..."
  wait-for-cassandra cassandra 120 cassandra cassandra
fi
coverage run -m nose2 tests
coverage report -m

