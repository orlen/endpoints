import logging
import typing as tp
import time
from dao import Failure

logger = logging.getLogger(__name__)

from tests import BaseTestCase


class TestFailure(BaseTestCase):

    def test_failure_creation(self):
        f1 = Failure.create('root@example.com', 'something-has-failed')
        time.sleep(2)   # so that they accidentally don't get the same timestamps
        f2 = Failure.create('root@example.com', 'something-has-failed', {'reason': 'wtf'})

        self.assertEquals(Failure('root@example.com', f1).reason, None)
        self.assertEquals(Failure('root@example.com', f2).reason, {'reason': 'wtf'})

