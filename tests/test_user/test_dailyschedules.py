from dao import DailyScheduled
from tests import BaseTestCase


class TestDailySchedules(BaseTestCase):
    LOGIN_ME_AS_WELL = True

    def test_add_and_get_and_delete_and_get(self):
        DailyScheduled.delete_all()

        r = self.put('/v1/schedules', {'drone_id': 'cr-drone-1',
                                       'hangar_id': 'cr-hangar-1',
                                       'user_id': 'root@example.com',
                                       'seconds': 3600})
        self.assertEquals(r.status_code, 201)
        r = self.get('/v1/schedules')
        self.assertEquals(r.json(), [{'drone_id': 'cr-drone-1',
                                      'hangar_id': 'cr-hangar-1',
                                      'user_id': 'root@example.com',
                                      'seconds': 3600}])
        r = self.delete('/v1/schedules/cr-hangar-1/3600')
        self.assertEquals(r.status_code, 200)
        r = self.get('/v1/schedules')
        self.assertEquals(r.json(), [])
