from dao import User
from tests import BaseTestCase


class TestUser(BaseTestCase):
    def test_login_and_logout(self):
        p = self.post('/v1/user/login', {'login': 'root@example.com',
                                         'password': '1234'})
        self.assertEquals(p.status_code, 200)
        p = self.post('/v1/user/logout', {})
        self.assertEquals(p.status_code, 200)

    def test_new_password(self):
        try:
            User.create('root2@example.com', '1234')
        except User.AlreadyExists:
            User('root2@example.com').set_password('1234')

        p = self.post('/v1/user/login', {'login': 'root2@example.com',
                                         'password': '1234'})
        self.post('/v1/user/new_password', {'new_password': '2345'})
        u = User('root2@example.com')
        self.assertTrue(u.check_password('2345'))
        u.delete()
