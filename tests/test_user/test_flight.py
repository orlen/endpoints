from tests import BaseTestCase


class TestFlight(BaseTestCase):
    LOGIN_ME_AS_WELL = True

    def test_list_schedules(self):
        """Test that endpoint /v1/flights/for works"""
        reply = self.post('/v1/flight/for', {'date': '2019-08-28'})
        self.assertEquals(reply.json()[0]['flight_id'], 'a530bfa5-2439-4253-93ce-c38082a476c1')
