import time
import unittest
import uuid

from dao import *


class TestFlight(unittest.TestCase):
    def test_delete_flight(self):
        """Check deleting a flight along with it's attached data files"""
        flight_id = Flight.create(0, 'cr-drone-1', 'cr-hangar-1', 'root@example.com', Flight.TOM_AUTOMATIC)
        flight = Flight(flight_id)
        datafile_id = DataFileAttached.create(flight, DataFileAttached.LOG, b'\x00\x00')
        flight.delete()
        self.assertRaises(Flight.DoesNotExist, lambda: Flight(flight_id))
        self.assertRaises(DataFileAttached.DoesNotExist, lambda: DataFileAttached(datafile_id))

    def test_stream_port(self):
        flight_id = Flight.create(0, 'cr-drone-1', 'cr-hangar-1', 'root@example.com', Flight.TOM_AUTOMATIC)
        flight = Flight(flight_id)
        self.assertIsNone(flight.stream_port)
        flight.stream_port = 8080
        flight = Flight(flight_id)
        self.assertEquals(flight.stream_port, 8080)


class TestDevice(unittest.TestCase):
    def test_delete_device(self):
        """Check deleting device along with it's attached device data files"""
        device_id = Device.create(uuid.uuid4().hex, 'crc32', 'auth_key', 'drone')
        device = Device(device_id)
        datafile_id = DeviceDataFileAttached.create(device, DeviceDataFileAttached.LOG, b'\x00\x00')
        device.delete()
        self.assertRaises(Device.DoesNotExist, lambda: Device(device_id))
        self.assertRaises(DeviceDataFileAttached.DoesNotExist, lambda: DeviceDataFileAttached(datafile_id))


class TestDatafile(unittest.TestCase):
    def test_update_hangar_and_drone(self):
        drone1 = Device(Device.create(uuid.uuid4().hex, 'crc32', 'auth_key', 'drone'))
        drone2 = Device(Device.create(uuid.uuid4().hex, 'crc32', 'auth_key', 'drone'))

        hangar1 = Device(Device.create(uuid.uuid4().hex, 'crc32', 'auth_key', 'hangar'))
        hangar2 = Device(Device.create(uuid.uuid4().hex, 'crc32', 'auth_key', 'hangar'))

        flight = Flight(Flight.create(10, drone1.device_id, hangar1.device_id, 'root@example.com', Flight.TOM_MANUAL))

        self.assertEquals(flight.drone.device_id, drone1.device_id)
        self.assertEquals(flight.hangar.device_id, hangar1.device_id)

        flight.drone = drone2
        flight.hangar = hangar2

        flight.refresh()

        self.assertEquals(flight.drone.device_id, drone2.device_id)
        self.assertEquals(flight.hangar.device_id, hangar2.device_id)

        flight.drone = drone1.device_id
        flight.hangar = hangar1.device_id

        flight.refresh()

        self.assertEquals(flight.drone.device_id, drone1.device_id)
        self.assertEquals(flight.hangar.device_id, hangar1.device_id)

    def test_delete_device(self):
        device = Device(Device.create(uuid.uuid4().hex, 'crc32', 'auth_key', 'drone'))

        datafile_id = DeviceDataFileAttached.create(device, DeviceDataFileAttached.LOG, b'\x00\x00')

        datafile = DeviceDataFileAttached(datafile_id)
        self.assertEquals(datafile.datafile_id, datafile_id)

        self.assertEquals(datafile.content, b'\x00\x00')

        device.refresh()
        self.assertEquals(device.device_datafile_ids, set([datafile_id]))

        datafile.delete()
        self.assertRaises(DeviceDataFileAttached.DoesNotExist, lambda: DeviceDataFileAttached(datafile_id))

        device.refresh()
        self.assertEquals(device.device_datafile_ids, set())

    def test_delete_flight(self):
        flight = Flight(Flight.create(0, 'cr-drone-1', 'cr-hangar-1', 'root@example.com', Flight.TOM_AUTOMATIC))

        datafile_id = DataFileAttached.create(flight, DataFileAttached.LOG, b'\x00\x00')

        datafile = DataFileAttached(datafile_id)

        self.assertEquals(datafile.content, b'\x00\x00')

        flight.refresh()
        self.assertEquals(flight.datafile_ids, set([datafile_id]))

        datafile.delete()
        self.assertRaises(DataFileAttached.DoesNotExist, lambda: DataFileAttached(datafile_id))

        flight.refresh()
        self.assertEquals(flight.datafile_ids, set())


class TestUser(unittest.TestCase):
    def test_create_user(self):
        self.assertTrue(User('root@example.com').check_password('1234'))

    def test_nonexistent_user(self):
        self.assertRaises(User.DoesNotExist, lambda: User('doesntexist@fake.com'))

    def test_already_exists(self):
        self.assertRaises(User.AlreadyExists, lambda: User.create('root@example.com', '1234'))


class TestSession(unittest.TestCase):
    def test_create_session(self):
        session_id = Session.create('root@example.com')
        session = Session(session_id)
        self.assertIsNone(session.expiry_time)
        session.delete()

    def test_session_expires(self):
        session_id = Session.create(User('root@example.com'), 1)
        time.sleep(1.1)
        self.assertRaises(Session.Expired, lambda: Session(session_id))

    def test_session_extends(self):
        session_id = Session.create('root@example.com', 1)
        session = Session(session_id)
        time.sleep(1.1)
        session.extend_for(2)
        Session(session_id)
