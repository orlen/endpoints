When passing an argument is mentioned, it is meant that it is meant to be passed
as a key of a JSON-encoded body, which is supposed to be a dict.


Note that everytime a time is mentioned, it refers to UTC!! Keep that in mind!

# Flight management

## Managing daily schedules

A **daily schedule model** is as follows:

```
{
    "seconds": seconds from the midnight (00:00) local-time-zone to launch the mission at,
    "drone_id": drone to perform the mission with, hardcode "cr-drone-1" for now
    "hangar_id": hangar to perform the mission with, hardcode "cr-hangar-1" for now
}
```

To retrieve list of currently defined models, just throw a GET at _/v1/schedules_. This will
return a list of daily schedule models.

To create a new daily schedule, just push a _PUT_ at _/v1/schedules/_ containing
all the fields that model specifies.

To delete a daily schedule, just throw a _DELETE_ at _/v1/schedules/<hangar_id>/<seconds>_

Stub the drone_id and hangar_id with *cr-drone-1* and *cr-hangar-1* respectively, for now.

## Flight model

A **flight model** is as follows

```
{
    "flight_id": 'id of the flight',
    "drone_id": 'id of the drone',
    "hangar_id": 'id of the hangar',
    "datetime": UNIX timestamp of the moment in time that this flight is scheduled for,
    "whom_approved": 'login of the user that approved this flight' or null,
    "type_of_mission": 0 for schedulable, 1 for manual
    "status": null for nothing, 0 for initializing, 1 for done,
                3 for cancelled or terminated
    "approved": null for not considered yet, 1 for approved, 2 for rejected,

}
```

## Obtain all flights scheduled for given datetime

/v1/flights/for

Pass *date* of format Y-m-d. Response will be a list of **flight model**.

# Login and logout

## login

/v1/user/login.

Pass *login* and *password*. You will get a cookie set in response.

## logout

/v1/user/logout

Will log-out the user. This will return a cookie-deleting response.

## change password

_/v1/user/new_password_

Pass *new_password*. This will not logout the user.

# Drone and hangar

## authorization

Use either _/v1/drone/authorize_app_ or _/v1/hangar/authorize_app_. As parameters
pass *device_id* and *checksum* which is Base64-encoded CRC32. In response you will receive
```
{
    "status": true or false,
    "key": Base64-encoded encryption key 
```

## drone flight initialization

Use _/v1/drone/initialize_ passing a dict of {device_id: drone_id} to get the following dict:

```
{
    "flight_id": ID of the flight,
    "route": exactly what you specified in path
}
```

If additional parametr of _chunk_ is specified, the _route_ argument will be sliced to 1500 bytes
and n-th chunk of the route will be fetched, starting from 0. You should finish downloading the chunks
if length of route equals to 0.

# drone and hangar alarm and log data upload

Use _/v1/(hangar|drone)/(dump_logs|alarm)/<string:device_id>_ with submitting me a single
multipart-encoded file. Please refer to [unit test](/tests/test_devices/test_log_upload.py)
in order to know how to do it.

# User's websocket

User is mandated to connect at _/v1/user/websocket_ and to be before authenticated by his cookie.
Protocol is JSON-encoded frames.

## Set stream URL to use

To obtain the URL of the stream that the user should view, just call POST _/v1/user/stream/_.
The response will look like:
```json
{
   "stream": "/stream.ogg"
}
```

If this ends with 404, then there are no flights in progress for this user.
If this ends with 409, then the stream is not yet ready.

With the _stream_ key precisioning which URL to use when retrieving the stream.


## Stream from drone

_message_type_s of _stream_ are Ctrl-C, Ctrl-v relayed to the user via his websocket. Look up to drone's documentation to figure them out.

## Flight confirmation

If a flight is required to be confirmed, the user will receive following message:
```json
{
  "type": "flight-confirm"
}
```

## Flight is rejected
If a flight is rejected by the hangar due to battery issues, the user will receive following message:
```json
{
  "type": "flight-reject"
}
```

## Flight is forced to be continued

```json
{
  "type": "forced-flight"
}
```
The protection against keeping the drone too long in wait mode has kicked in.

## Flight is finished
If a flight is finished, the user will receive the following message from the websocket:
```json
{
  "type": "flight-finished"
}
```

Alternatively user can inquire at any moment using the _/v1/user/flights_outstanding_ endpoint.
It shall return a dict of 
```json
{
  "flights_outstanding": true/false
}
```

If there are any flights to confirm.

## User commands

User commands are sent to endpoint _/v1/user/commands_ with a single dict containing value:

```
{
    "order": "authorize" to permit for the flight to take place
             "cancel" to cancel pending flight
             "stop" to tell the drone to suspend the flight
             "resume" to tell the drone to carry out
             "abort" to tell the drone to return home
}
```

A return is a dict containing a single key _status_ which is a bool, standing for
whether the operation completed successfully.

## Manual flight creation

To manually schedule a new flight, call _/v1/flight/new_ with PUT method with the following values:

```
{
    "drone_id": "cr-drone-1",
    "hangar_id": "cr-hangar-1"
}
```

A 200 and an empty dict is supposed to be returned.

## List of failures

To get a list of failures, just query _/v1/user/failures_. You will receive a list
of dicts that look like this

{
  "user_id": str, user to which the message was targeted,
  
  "datetime": int, a GMT+0 timestamp in seconds
  
  "failure": str, [failure message](failures.md) message of the failure
}

Failures are kept for only the last 24 hours.

## Delete all failures

To delete all failures for given user, just use a _DELETE_ call on _/v1/user/failures_.
