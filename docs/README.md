# Development

Run [docker-compose-unittests.yml](docker-compose-unittests.yml) service unittest for unit tests.
This has
zero requirements and will build everything from scratch.

If you ever wanted to run it in production, use 
[docker-compose-production.yml](docker-compose-production.yml).
It will build everything and expose you the Endpoints API on port 80.

However, read [envs](docs/environment.md) first.

Keep in mind that if your blueprint's name ends with " WS" (note the space), it will be treated
as a websocket, else it will be treated as a normal Flask endpoint.

# Topic of contents

1. [Endpoints](endpoints.md) - list of endpoints along with their specification
2. [Variables](variables.md) - database specification, transient things
3. [Environment](environment.md) - environment variables that need to be set for Endpoints to work
