# Hangar

## HTTPS Endpoints - Client
### Odszyfrowywanie aplikacji
* Type: **HTTP GET**
* Endpoint: `/v1/hangar/authorize_app`
* Request body:
```
{
    'device_id': string,
    'checksum': string
}
```
* Response body:
```
{
    'status': bool,
    'key': string or null
}
```

### Zrzut logów
* Type: **HTTP POST**
* Endpoint: `/v1/hangar/dump_logs`
* Request body:
```
{
    'device_id': string,
}
+ form-data/multipart
```

### Wysyłanie alarmu
* Type: **HTTP POST**
* Endpoint: `/v1/hangar/alarm`
* Request body:
```
{
    'device_id': string,
    ?????
}
```

## WebSocket

* Type: **WS**
* Endpoint: `/v1/hangar/websocket/<device_id>`

### Komendy  (Server -> Device):
Server Header:
```
{
    'timestamp': int,
    'name': string,
    'operator_id': string,
    'parameters': dict
}
```

#### Lista komend:
| Nazwa komendy | Parametry | Opis |
|-|-|-|
| init_flight | ```"parameters": {"flight_id": "1"}``` | Inicjuje przelot |
| cancel_flight | ```"parameters": {}``` | Anuluje przygotowywanie lotu |
| drone_started | ```"parameters": {}``` | Informuje hangar że dron wystartował i znajduje sie w jegos trefie manewrowej, hangar powinien byc gotowy na przyjecie drona |
| leave_drone | ```"parameters": {}``` | Informuje hangar ze dron opuscił strefe manewrową hangaru i udziła tego hangaru w przelocie sie kończy |
| receive_drone | ```"parameters": {"flight_id": "1", "drone_id": "115"}``` | Informuje hangar ze ma przygotowac sie do odebrania drona |
| drone_landed | ```"parameters": {}``` | Informuje hangar ze dron wylądował i hangar moze rozpoczac jego obsługę | 
| shutdown_drone | ```"parameters": {}``` | Informuje hangar ze dron wylądował i hangar moze rozpoczac jego obsługę | 
| | | |
| alive | ```"parameters": {}``` | Chmura żąda od hangaru informacje o jego stanie ("hangar_heart_beat")|
|service_finish_flight| ```"parameters": {"flight_id": "1", "drone_id": "115"}``` | Chmura żada od hangaru zmankiecie lotu i sztuczne przyjecie dorna na stan |

### Wiadomosci (Device -> Server):
Client Header:
```
{
    'timestamp': int,
    'device_id': string,
    'message_id': string,
    'message_type' string,
    <message_type>: dict
}
```

W zależnosci od <message_type>:
#### Command Response
```
'command_response': {
    'operator_id': string,
    'command_name': string,
    'command_timestamp': int,
    'command_status': string,
    'command_parameters': dict
}
```

#### State Change
```
'state_change': {
    'old_state': string,
    'new_state': string,
}
```

#### Heartbeat
```
'heartbeat` {
    'drone_id': string or None,
    'state': string,
    'drone_battery': {
        'charging': bool,
        'voltage': float or None
        'current': float or None
    } or None
}
```