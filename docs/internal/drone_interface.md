# Hangar

## HTTPS Endpoints - Client
### Odszyfrowywanie aplikacji
* Type: **HTTP GET**
* Endpoint: `/v1/hangar/authorize_app`
* Request body:
```
{
    'device_id': string,
    'checksum': string
}
```
* Response body:
```
{
    'status': bool,
    'key': string or null
}
```

### Zrzut logów
* Type: **HTTP POST**
* Endpoint: `/v1/hangar/dump_logs`
* Request body:
```
{
    'device_id': string,
}
+ form-data/multipart
```

### Wysyłanie alarmu
* Type: **HTTP POST**
* Endpoint: `/v1/hangar/alarm`
* Request body:
```
{
    'device_id': string.
    ?????
}
```

### Wake Up
Zostanie przygotowane jak najszybciej w bliżej nieokreślonej przyszłosci


## WebSocket
* Type: **WS**
* Endpoint: `/v1/hangar/websocket/<flight_id>`

### Komendy  (Server -> Device):
#### Nagłówek:
Server Header:
```
{
    'timestamp': int,
    'name': string,
    'operator_id': string,
    'parameters': dict
}
```

#### Lista komend:
Zostanie przygotowane jak najszybciej w bliżej nieokreślonej przyszłosci


### Wiadomosci (Device -> Server):
Client Header:
```
{
    'timestamp': int,
    'device_id': string,
    'message_id': string,
    'message_type' string,
    <message_type>: dict
}
```

W zależnosci od <message_type>:
#### Command Response
```
'command_response': {
    'operator_id': string,
    'command_name': string,
    'command_timestamp': int,
    'command_status': string,
    'command_parameters': dict
}
```

#### State Change
```
'state_change': {
    'old_state': string,
    'new_state': string,
}
```

#### Stream
```
'stream':
    'system': {
        'satellites_visible': int,
        'state' string,
        'route_id': string
    },
    'position': {
        'gps_lat' float,
        'gps_lon': float,
        'alt_local': float,
        'vel_ned': [float, float, float],
        'yaw': float,
        'quat': [float, float, float, float],
        'hdop': float
    },
    'waypoint': {
        'next_point': int,
        'progress': float
    },
    'battery': {
        'remaining': float,
        'voltage': float,
        'current': float
    },
    'ekf': {
        'velocity_variance': float,
        'pos_horiz_variance': float,
        'pos_vert_variance': float,
        'compass_variance': float
    }
}
```
