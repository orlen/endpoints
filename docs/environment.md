# Environment variables defined

* **CASSANDRA_HOST** - the host Cassandra listens on. By default *cassandra*. \
* **CASSANDRA_KEYSPACE** - keyspace to use on target Cassandra. Defaults to *api*
* **CASSANDRA_USERNAME** - username to use for authentication with Cassandra. Default is *cassandra*
* **CASSANDRA_PASSWORD** - password to use for authentication with Cassandra. Default is *cassandra*
* **CASSANDRA_DC** - name of the local DC. By default *datacenter1*, as such is the default in Cassandra
* **REDIS_HOST** - host to the Redis, default is *redis*
* **REDIS_PORT** - port to connect to Redis, default is 6379
* **REDIS_DB** - Redis database to use, default is 0 
* **DEBUG** - Either **True** or **False** (capitalization matters), default is True
* **VIDEO_SAVE_PATH** - path to save the videos to, default is */videos*, which is where the volume with videos
  should be mounted too
