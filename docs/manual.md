# Operator wishes to assert manual control over the drone

If the operator wishes to assert manual control over the drone,
the drone should enter state of `manual_control`.

In this case a command of `manual_close` will be dispatched to the hangar.
