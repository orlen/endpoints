
# Variables defined within Cassandra

The table used is called [kvs](tests/dockerfiles/cassandra/cassandra.cql).

* **[drone.path](devices/authorization.py)** - this is a JSON that is served on /v1/user/path
* **[cron.last_day_schedule_generated_for](devices/schedule_cron.py)** - UNIX timestamp of the beginning of the last day that flights were defined from from daily schedules
* **[default.drone.source](videograbber/videograbber_control.py)** - used to determine what RSTP host the drone is

# Variables defined within Redis

* **USER.<user_id>.FLIGHT** - Flight currently being managed by an user
* **DRONE.<device id>.FLIGHT** - UUID of the flight a drone is currently executing. This is assigned on flight init.
* **HANGAR.<device_id>.FLIGHT** - Flight currently being managed by the hangar
* **HANGAR.<device_id>.BATTERY** - 1 if charged, 0 else
* **FLIGHT.MANAGED.<user id>** - ID of flight managed by the current user
* **FLIGHTS** - hashset whose keys are current Flight IDs
* **HANGAR.<device_id>.STATE** - current state of the hangar
* **DRONE.<device_id>.STATE** - current state of the drone
* **FLIGHT.<flight_id>.SECONDS_REMAINING_IN_WAIT** - seconds remaining for the wait mode for given flight
* **FLIGHT.<flight_id>.LAST_CHECKED** - timestamp of when seconds remaining in wait was last processed
