# Failures are as follows

Failures are logged for given user. They are timestamped. They consist
of a primary called _failure_, and an additional JSON-able dictionary
called _reason_. They are read using _/v1/user/failures_. After reading them
you should delete them. Do this calling method [DELETE](endpoints.md) on resource
_/v1/user/failures_.

* **hangar.disconnected** - an attempt was made to initialize a flight, but given hangar was offline
    * reason will be a dict with one key of:
        * reason
            * no-websocket : no websocket for this hangar has reported since
            * websocket-closed : websocket has been closed
* **battery.not.charged** - hangar reported battery not being ready
* **flight.running** - another flight is in progress, a flight has been cancelled
