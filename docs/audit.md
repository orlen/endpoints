# Audit rules

# Drone and hangar comms

These are logged by their respective _device_ids_.

# User events

Following journals are additionally kept. If an **user** field
is present, it signifies the user who introduced the change.

* **accounts**
    * With key **what_happened** equal to **password changed** having following keys:
        * user: str
* **schedules**
    * With key **what_happened** equal to **added**:
        * user: str
        * seconds: int
        * drone_id: str
        * hangar_id: int
        * user_id: str - user to which this flight is bound
    * With key **what_happened** equal to **removed**:
        * seconds: integer
        * hangar_id: str
        * user: str
    * With key **what_happened** equal to **manually_triggered**:
        * user: str
* **security**
    * With key **what_happened** equal to **wrong_login**
        * reason: str:
            * 'invalid_user' for nonexistent or user not provided
            * 'invalid_password' for password mismatch  
        * ip: str - IP of the remote peer
        * login: optional[str] - the login the user tried to use
    * With key **what_happened** equal to **login**
        * user: str
        * ip: str
    * With key **what_happened** equal to **logout**
        * user: str
        * ip: str
    * With key **what_happened** equal to **bad_session**
        * user: optional[str] - the offending user, if available
        * url: str
        * method: str
        * ip: str
        * reason: str, one of
            * session_id_not_given
            * session_not_found
            * session_expired
            * user_deleted

    